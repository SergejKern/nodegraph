﻿namespace nodegraph.Editor.Enums
{
    internal enum NodeActivity
    {
        Idle,
        HoldNode,
        DragNode,
        HoldGrid,
        DragGrid
    }
}
