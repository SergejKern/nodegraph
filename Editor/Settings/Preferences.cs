﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using nodegraph.Editor.Attributes;
using nodegraph.Editor.Enums;
using nodegraph.Editor.Interface;
using nodegraph.Editor.Utility;
using Random = UnityEngine.Random;

namespace nodegraph.Editor.Settings
{
    public static class NodeEditorPreferences
    {
        /// <summary> The last editor we checked. This should be the one we modify </summary>
        static INodeGraphEditor m_lastEditor;
        /// <summary> The last key we checked. This should be the one we modify </summary>
        static string m_lastKey = "nodegraph.Settings";

        static Dictionary<Type, Color> m_typeColors = new Dictionary<Type, Color>();
        static readonly Dictionary<string, Settings> k_settings = new Dictionary<string, Settings>();

        /// <summary> Get settings of current active editor </summary>
        public static Settings GetSettings()
        {
            if (NodeEditorWindow.Current == null) 
                return new Settings();

            if (m_lastEditor != NodeEditorWindow.Current.GraphEditor)
            {
                var attributes = NodeEditorWindow.Current.GraphEditor.GetType()
                    .GetCustomAttributes(typeof(CustomNodeGraphEditorAttribute), true);
                if (attributes.Length == 1)
                {
                    var attribute = attributes[0] as CustomNodeGraphEditorAttribute;
                    m_lastEditor = NodeEditorWindow.Current.GraphEditor;
                    m_lastKey = attribute?.EditorPrefsKey;
                }
                else return null;
            }
            if (!k_settings.ContainsKey(m_lastKey ?? throw new InvalidOperationException())) 
                VerifyLoaded();
            return k_settings[m_lastKey];
        }

        [SettingsProvider]
        public static SettingsProvider CreateXNodeSettingsProvider()
        {
            var provider = new SettingsProvider("Preferences/Node Editor", SettingsScope.User)
            {
                guiHandler = (searchContext) => { NodeEditorPreferences.PreferencesGUI(); },
                keywords = new HashSet<string>(new[] { "xNode", "node", "editor", "graph", "connections", "noodles", "ports" })
            };
            return provider;
        }

        static void PreferencesGUI()
        {
            VerifyLoaded();
            var settings = k_settings[m_lastKey];

            if (GUILayout.Button(new GUIContent("Documentation", "https://github.com/Siccity/xNode/wiki"), GUILayout.Width(100))) 
                Application.OpenURL("https://github.com/Siccity/xNode/wiki");
            EditorGUILayout.Space();

            NodeSettingsGUI(m_lastKey, settings);
            GridSettingsGUI(m_lastKey, settings);
            SystemSettingsGUI(m_lastKey, settings);
            TypeColorsGUI(m_lastKey, settings);
            if (GUILayout.Button(new GUIContent("Set Default", "Reset all values to default"), GUILayout.Width(120)))
                ResetPrefs();
        }

        static void GridSettingsGUI(string key, Settings settings)
        {
            //Label
            EditorGUILayout.LabelField("Grid", EditorStyles.boldLabel);
            settings.GridSnap = EditorGUILayout.Toggle(new GUIContent("Snap", "Hold CTRL in editor to invert"), settings.GridSnap);
            settings.ZoomToMouse = EditorGUILayout.Toggle(new GUIContent("Zoom to Mouse", "Zooms towards mouse position"), settings.ZoomToMouse);
            EditorGUILayout.LabelField("Zoom");
            EditorGUI.indentLevel++;
            settings.MaxZoom = EditorGUILayout.FloatField(new GUIContent("Max", "Upper limit to zoom"), settings.MaxZoom);
            settings.MinZoom = EditorGUILayout.FloatField(new GUIContent("Min", "Lower limit to zoom"), settings.MinZoom);
            EditorGUI.indentLevel--;
            settings.GridLineColor = EditorGUILayout.ColorField("Color", settings.GridLineColor);
            settings.GridBgColor = EditorGUILayout.ColorField(" ", settings.GridBgColor);
            if (GUI.changed)
            {
                SavePrefs(key, settings);
                NodeEditorWindow.RepaintAll();
            }

            EditorGUILayout.Space();
        }

        static void SystemSettingsGUI(string key, Settings settings)
        {
            //Label
            EditorGUILayout.LabelField("System", EditorStyles.boldLabel);
            settings.AutoSave = EditorGUILayout.Toggle(new GUIContent("Autosave", "Disable for better editor performance"), settings.AutoSave);
            settings.OpenOnCreate = EditorGUILayout.Toggle(new GUIContent("Open Editor on Create", "Disable to prevent opening the editor when creating a new graph"), settings.OpenOnCreate);
            if (GUI.changed) 
                SavePrefs(key, settings);

            EditorGUILayout.Space();
        }

        static void NodeSettingsGUI(string key, Settings settings)
        {
            EditorGUILayout.LabelField("Node", EditorStyles.boldLabel);
            settings.HighlightColor = EditorGUILayout.ColorField("Selection", settings.HighlightColor);
            settings.ActiveColor = EditorGUILayout.ColorField("Active", settings.ActiveColor);

            settings.NoodlePath = (NoodlePath) EditorGUILayout.EnumPopup("Noodle path", settings.NoodlePath);
            settings.NoodleStroke = (NoodleStroke) EditorGUILayout.EnumPopup("Noodle stroke", settings.NoodleStroke);
            settings.PortTooltips = EditorGUILayout.Toggle("Port Tooltips", settings.PortTooltips);
            settings.DragToCreate = EditorGUILayout.Toggle(new GUIContent("Drag to Create", "Drag a port connection anywhere on the grid to create and connect a node"), settings.DragToCreate);
            settings.DrawOnlyPorts = EditorGUILayout.Toggle("Draw port-fields only", settings.DrawOnlyPorts);

            if (GUI.changed)
            {
                SavePrefs(key, settings);
                NodeEditorWindow.RepaintAll();
            }
            EditorGUILayout.Space();
        }

        static void TypeColorsGUI(string key, Settings settings)
        {
            //Label
            EditorGUILayout.LabelField("Types", EditorStyles.boldLabel);

            //Clone keys so we can enumerate the dictionary and make changes.
            var typeColorKeys = new List<Type>(m_typeColors.Keys);

            //Display type colors. Save them if they are edited by the user
            foreach (var type in typeColorKeys)
            {
                var typeColorKey = type.PrettyName();
                var col = m_typeColors[type];
                EditorGUI.BeginChangeCheck();
                EditorGUILayout.BeginHorizontal();
                col = EditorGUILayout.ColorField(typeColorKey, col);
                EditorGUILayout.EndHorizontal();

                if (!EditorGUI.EndChangeCheck()) 
                    continue;

                m_typeColors[type] = col;

                if (settings.TypeColors.ContainsKey(typeColorKey)) 
                    settings.TypeColors[typeColorKey] = col;
                else 
                    settings.TypeColors.Add(typeColorKey, col);

                SavePrefs(key, settings);
                NodeEditorWindow.RepaintAll();
            }
        }

        /// <summary> Load prefs if they exist. Create if they don't </summary>
        static Settings LoadPrefs()
        {
            // Create settings if it doesn't exist
            if (EditorPrefs.HasKey(m_lastKey)) 
                return JsonUtility.FromJson<Settings>(EditorPrefs.GetString(m_lastKey));

            EditorPrefs.SetString(m_lastKey,
                m_lastEditor != null
                    ? JsonUtility.ToJson(m_lastEditor.GetDefaultPreferences())
                    : JsonUtility.ToJson(new Settings()));
            return JsonUtility.FromJson<Settings>(EditorPrefs.GetString(m_lastKey));
        }

        /// <summary> Delete all prefs </summary>
        static void ResetPrefs()
        {
            if (EditorPrefs.HasKey(m_lastKey)) 
                EditorPrefs.DeleteKey(m_lastKey);
            if (k_settings.ContainsKey(m_lastKey)) 
                k_settings.Remove(m_lastKey);

            m_typeColors = new Dictionary<Type, Color>();
            VerifyLoaded();
            NodeEditorWindow.RepaintAll();
        }

        /// <summary> Save preferences in EditorPrefs </summary>
        static void SavePrefs(string key, Settings settings) 
            => EditorPrefs.SetString(key, JsonUtility.ToJson(settings));

        /// <summary> Check if we have loaded settings for given key. If not, load them </summary>
        static void VerifyLoaded()
        {
            if (k_settings.ContainsKey(m_lastKey)) 
                return;
            
            k_settings.Add(m_lastKey, LoadPrefs());
        }

        /// <summary> Return color based on type </summary>
        public static Color GetTypeColor(Type type)
        {
            VerifyLoaded();
            if (type == null) 
                return Color.gray;

            if (m_typeColors.TryGetValue(type, out var col)) 
                return col;
            var typeName = type.PrettyName();
            if (k_settings[m_lastKey].TypeColors.ContainsKey(typeName)) 
                m_typeColors.Add(type, k_settings[m_lastKey].TypeColors[typeName]);
            else
            {
                var oldState = Random.state;
                Random.InitState(typeName.GetHashCode());

                col = new Color(Random.value, Random.value, Random.value);
                m_typeColors.Add(type, col);
                Random.state = oldState;
            }
            return col;
        }
    }
}