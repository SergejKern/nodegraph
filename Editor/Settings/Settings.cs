﻿using System;
using System.Collections.Generic;
using UnityEngine;
using nodegraph.Editor.Enums;
using nodegraph.Editor.Resources;

namespace nodegraph.Editor.Settings
{

    [Serializable]
    public class Settings : ISerializationCallbackReceiver
    {
        [SerializeField] Color32 m_gridLineColor = new Color(0.45f, 0.45f, 0.45f);
        public Color32 GridLineColor
        {
            get => m_gridLineColor;
            set
            {
                m_gridLineColor = value;
                m_gridTexture = null;
                m_crossTexture = null;
            }
        }

        [SerializeField] Color32 m_gridBgColor = new Color(0.18f, 0.18f, 0.18f);
        public Color32 GridBgColor
        {
            get => m_gridBgColor;
            set
            {
                m_gridBgColor = value;
                m_gridTexture = null;
            }
        }

        public float MaxZoom = 5f;
        public float MinZoom = 1f;
        public Color32 HighlightColor = new Color32(255, 255, 255, 255);
        public Color32 ActiveColor = new Color32(155, 255, 155, 255);

        public bool GridSnap = true;
        public bool AutoSave = true;
        public bool OpenOnCreate = true;
        public bool DragToCreate = true;
        public bool ZoomToMouse = true;
        public bool PortTooltips = true;
        public bool DrawOnlyPorts = true;

        [SerializeField] string m_typeColorsData = "";
        [NonSerialized] public Dictionary<string, Color> TypeColors = new Dictionary<string, Color>();

        public NoodlePath NoodlePath = NoodlePath.Curvy;
        public NoodleStroke NoodleStroke = NoodleStroke.Full;

        Texture2D m_gridTexture;
        public Texture2D GridTexture
        {
            get
            {
                if (m_gridTexture == null)
                    m_gridTexture = NodeEditorResources.GenerateGridTexture(GridLineColor, GridBgColor);
                return m_gridTexture;
            }
        }

        Texture2D m_crossTexture;
        public Texture2D CrossTexture
        {
            get
            {
                if (m_crossTexture == null)
                    m_crossTexture = NodeEditorResources.GenerateCrossTexture(GridLineColor);
                return m_crossTexture;
            }
        }

        public void OnAfterDeserialize()
        {
            // Deserialize typeColorsData
            TypeColors = new Dictionary<string, Color>();
            var data = m_typeColorsData.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            for (var i = 0; i < data.Length; i += 2)
            {
                if (ColorUtility.TryParseHtmlString("#" + data[i + 1], out var col))
                    TypeColors.Add(data[i], col);
            }
        }

        public void OnBeforeSerialize()
        {
            // Serialize typeColors
            m_typeColorsData = "";
            foreach (var item in TypeColors)
                m_typeColorsData += item.Key + "," + ColorUtility.ToHtmlStringRGB(item.Value) + ",";
        }
    }
}