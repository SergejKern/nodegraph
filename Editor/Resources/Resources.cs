﻿using UnityEditor;
using UnityEngine;
using nodegraph.Editor.Data;

namespace nodegraph.Editor.Resources
{
    public static class NodeEditorResources
    {
        #region Textures
        // ReSharper disable StringLiteralTypo
        public static Texture2D Dot => m_dot != null 
            ? m_dot 
            : m_dot = UnityEngine.Resources.Load<Texture2D>("xnode_dot");
        static Texture2D m_dot;

        public static Texture2D DotOuter => m_dotOuter != null 
            ? m_dotOuter 
            : m_dotOuter = UnityEngine.Resources.Load<Texture2D>("xnode_dot_outer");
        static Texture2D m_dotOuter;

        public static Texture2D NodeBody => m_nodeBody != null
            ? m_nodeBody 
            : m_nodeBody = UnityEngine.Resources.Load<Texture2D>("xnode_node");
        static Texture2D m_nodeBody;

        public static Texture2D NodeHighlight => m_nodeHighlight != null 
            ? m_nodeHighlight 
            : m_nodeHighlight = UnityEngine.Resources.Load<Texture2D>("xnode_node_highlight");
        static Texture2D m_nodeHighlight;
        // ReSharper restore StringLiteralTypo
        #endregion

        // Styles
        public static NodeStyles Styles
        {
            get
            {
                if (!m_styles.IsValid) m_styles = NodeStyles.Default;
                return m_styles;
            }
        }

        static NodeStyles m_styles;
        public static GUIStyle OutputPort => new GUIStyle(EditorStyles.label) { alignment = TextAnchor.UpperRight };

        public static Texture2D GenerateGridTexture(Color line, Color bg)
        {
            var tex = new Texture2D(64, 64);
            var cols = new Color[64 * 64];
            for (var y = 0; y < 64; y++)
            {
                for (var x = 0; x < 64; x++)
                {
                    var col = bg;

                    if (y % 16 == 0 || x % 16 == 0) 
                        col = Color.Lerp(line, bg, 0.65f);
                    if (y == 63 || x == 63) 
                        col = Color.Lerp(line, bg, 0.35f);

                    cols[(y * 64) + x] = col;
                }
            }

            tex.SetPixels(cols);
            tex.wrapMode = TextureWrapMode.Repeat;
            tex.filterMode = FilterMode.Bilinear;
            tex.name = "Grid";
            tex.Apply();
            return tex;
        }

        public static Texture2D GenerateCrossTexture(Color line)
        {
            var tex = new Texture2D(64, 64);
            var cols = new Color[64 * 64];
            for (var y = 0; y < 64; y++)
            {
                for (var x = 0; x < 64; x++)
                {
                    var col = line;
                    if (y != 31 && x != 31) 
                        col.a = 0;
                    cols[(y * 64) + x] = col;
                }
            }

            tex.SetPixels(cols);
            tex.wrapMode = TextureWrapMode.Clamp;
            tex.filterMode = FilterMode.Bilinear;
            tex.name = "Grid";
            tex.Apply();
            return tex;
        }
    }
}