﻿using System;
using System.IO;
using System.Linq;
using Core.Editor.Utility;
using UnityEditor;
using UnityEngine;
using nodegraph.Attributes;
using nodegraph.Editor.Operations;

namespace nodegraph.Editor.Processor
{
    /// <summary> Deals with modified assets </summary>
    internal class NodeGraphImporter : AssetPostprocessor
    {
        const string k_assetExt = ".asset";
        const int k_positionXOffset = 200;

        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            foreach (var path in importedAssets)
            {
                // Skip processing anything without the .asset extension
                if (Path.GetExtension(path) != k_assetExt) continue;

                var pathType = AssetDatabase.GetMainAssetTypeAtPath(path);
                if (!typeof(INodeGraph).IsAssignableFrom(pathType))
                    return;

                var assets = AssetDatabase.LoadAllAssetsAtPath(path);
                foreach (var asset in assets)
                {
                    // Get the object that is requested for deletion
                    if (!(asset is INodeGraph graph))
                        continue;

                    // Get attributes
                    var graphType = graph.GetType();
                    var attributes = Array.ConvertAll(
                        graphType.GetCustomAttributes(typeof(RequireNodeAttribute), true), x => x as RequireNodeAttribute);

                    var position = Vector2.zero;
                    foreach (var attribute in attributes)
                    {
                        if (attribute.Type0 != null) 
                            AddRequired(graph, attribute.Type0, ref position);
                        if (attribute.Type1 != null) 
                            AddRequired(graph, attribute.Type1, ref position);
                        if (attribute.Type2 != null) 
                            AddRequired(graph, attribute.Type2, ref position);
                    }
                }
            }
        }

        static void AddRequired(INodeGraph graph, Type type, ref Vector2 position)
        {
            if (graph.NodeObjs.Any(x => x.GetType() == type)) 
                return;
            
            var node = graph.AddNode(type);
            node.Position = position;
            position.x += k_positionXOffset;

            if (node.name.Trim() == "")
                node.name = node.DefaultName();

            var parent = graph.Parent();
            if (!string.IsNullOrEmpty(parent.Path()))
                node.AddToAsset(parent);
        }
    }
}