﻿using Core.Editor.Inspector;
using UnityEditor;
using UnityEngine;

namespace nodegraph.Editor.Inspector
{
    /// <summary> Override graph inspector to show an 'Open Graph' button at the top </summary>
    [CustomEditor(typeof(INodeGraph), true)]
    [CanEditMultipleObjects]
    public class GraphInspector : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            if (GUILayout.Button("Edit graph", GUILayout.Height(40)))
                NodeEditorWindow.Open(serializedObject.targetObject as INodeGraph);

            GUILayout.Space(EditorGUIUtility.singleLineHeight);
            GUILayout.Label("Raw data", "BoldLabel");

            DrawDefaultInspector();

            serializedObject.ApplyModifiedProperties();
        }
    }

    [CustomEditor(typeof(Node), true)]
    [CanEditMultipleObjects]
    public class NodeInspector : BaseInspector<NodeInspectorEditor>
    {
        public override bool CanDetach => false;
    }

    public class NodeInspectorEditor : BaseEditor<Node>
    {
        public override void OnGUI(float width)
        {
            SerializedObject.Update();

            if (NodeEditorWindow.Current == null
                || !NodeEditorWindow.Current.IsVisible
                || NodeEditorWindow.Current.Graph != Target.Graph)
            {
                if (GUILayout.Button("Edit graph", GUILayout.Height(40)))
                {
                    var graphProp = SerializedObject.FindProperty(Node.GraphPropName);
                    //Debug.Log($"{graphProp} {graphProp?.objectReferenceValue}");
                    var w = NodeEditorWindow.Open(graphProp.objectReferenceValue as INodeGraph);
                    w.ResetView(); // Focus selected node
                }
            }


            GUILayout.Space(EditorGUIUtility.singleLineHeight);
            GUILayout.Label("Raw data", "BoldLabel");

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                // Now draw the node itself.
                if (ParentContainer is UnityEditor.Editor ed) 
                    ed.DrawDefaultInspector();

                if (check.changed)
                    OnChanged();
            }

            SerializedObject.ApplyModifiedProperties();
        }
    }

    [CustomEditor(typeof(WrapperNode), true)]
    [CanEditMultipleObjects]
    public class WrapperNodeInspector : UnityEditor.Editor
    {
        UnityEditor.Editor m_dataEditor;

        new WrapperNode target => (WrapperNode) base.target;

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            if (GUILayout.Button("Edit graph", GUILayout.Height(40)))
            {
                var graphProp = serializedObject.FindProperty(Node.GraphPropName);
                //Debug.Log($"{graphProp} {graphProp?.objectReferenceValue}");
                var w = NodeEditorWindow.Open(graphProp.objectReferenceValue as INodeGraph);
                w.ResetView(); // Focus selected node
            }

            GUILayout.Space(EditorGUIUtility.singleLineHeight);
            GUILayout.Label("Raw data", "BoldLabel");

            CreateCachedEditor((Object) target.NodeData,null, ref m_dataEditor);
            m_dataEditor.OnInspectorGUI();

            serializedObject.ApplyModifiedProperties();
        }
    }
}