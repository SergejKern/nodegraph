﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Editor.Tools;
using Core.Editor.Utility;
using Core.Extensions;
using Core.Interface;
using Core.Types;
using Core.Unity.Extensions;
using nodegraph.Data;
using nodegraph.Editor.Operations;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

using SO = UnityEngine.ScriptableObject;
namespace nodegraph.Editor.Popup
{
    public class CreateNestedGraphWindow : EditorWindow
    {
        struct NodeData
        {
            public bool Checked;
            public bool IsExpanded;

            public INode Node;
            public NodesPortData[] Ports;
        }

        struct NodesPortData
        {
            public PortID PortID;
            public NodePort MapToPortTemplate;

            public bool Checked;
        }

        struct PortMapping
        {
            public NodePort Template;

            public NodePort OuterPort;
            public NodePort InnerPort;
        }

        INodeGraph m_originalGraph;
        NodeData[] m_nodes;
        IEnumerable<INode> m_selection;

        readonly List<PortMapping> m_nodePorts = new List<PortMapping>();

        string m_newNodeNameContainingGraph;
        string m_nestedGraphPath;

        Vector2 m_scrollPos;

        bool m_nodesExpanded;
        bool m_mappedPortsExpanded;

        #region Init
        public static CreateNestedGraphWindow Show(INodeGraph graph, IEnumerable<INode> selection)
        {
            var menu = CreateInstance<CreateNestedGraphWindow>();
            menu.titleContent = new GUIContent("Create Nested Graph");
            menu.PopulateData(graph, selection);
            menu.minSize = new Vector3(600, 300);
            menu.maxSize = new Vector3(600, 1000);
            menu.Show();

            return menu;
        }

        void PopulateData(INodeGraph graph, IEnumerable<INode> selection)
        {
            m_originalGraph = graph;
            m_selection = selection;

            m_nodePorts.Clear();
            m_nodes = new NodeData[graph.NodeObjs.Length];

            for (var i = 0; i < graph.NodeObjs.Length; i++)
                CreateNodeData(graph, i);
        }

        void CreateNodeData(INodeGraph graph, int i)
        {
            var node = (INode)graph.NodeObjs[i];
            var ports = node.Ports();

            var selectedNode = m_selection.Contains(node);
            m_nodes[i] = new NodeData()
            {
                Node = node,
                Ports = CreatePortData(ports, node),
                Checked = selectedNode,
            };
        }

        NodesPortData[] CreatePortData(IEnumerable<NodePort> ports, INode node)
        {
            var selectedNode = m_selection.Contains(node);
            var portData = new NodesPortData[ports.Count()];

            var i = 0;
            foreach (var p in ports)
            {
                GetMappedPort(node, selectedNode, p,
                    out var mapTo, out var selected);

                portData[i] = new NodesPortData()
                {
                    Checked = selected,
                    PortID = p.ID,
                    MapToPortTemplate = mapTo
                };
                ++i;
            }

            return portData;
        }

        void GetMappedPort(INamed node, bool selectedNode, NodePort initialPort, out NodePort mapTo,
            out bool selected)
        {
            mapTo = null;
            selected = selectedNode && initialPort.IsConnected && initialPort.HasOutwardConnection(m_selection);
            if (!selected)
                return;

            var uniqueName = GetUniqueName(node, initialPort);
            mapTo = new NodePort(uniqueName, initialPort.ValueType, initialPort.Direction, initialPort.ConnectionType, initialPort.TypeConstraint, null);
            m_nodePorts.Add(new PortMapping() { Template = mapTo });
        }

        string GetUniqueName(INamed node, NodePort p)
        {
            var fullPortName = $"{node.Name}_{p.FieldName}";
            while (m_nodePorts.FindIndex(np => string.Equals(np.Template.FieldName, fullPortName)) != -1)
                fullPortName = $"{node.Name}_{p.FieldName} {100 * Random.value:####}";

            return fullPortName;
        }

        #endregion

        public void OnGUI()
        {
            using (var scroll = new GUILayout.ScrollViewScope(m_scrollPos, false, true))
            {
                m_scrollPos = scroll.scrollPosition;
                NameAndPathGUI();

                NodesGUI();
                MappedPortsGUI();
            }

            OKCancelGUI();
        }

        void NameAndPathGUI()
        {
            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                m_newNodeNameContainingGraph = EditorGUILayout.TextField("Node Name: ", m_newNodeNameContainingGraph);
                using (new EditorGUILayout.HorizontalScope())
                {
                    var choosePath = GUILayout.Button("...", GUILayout.Width(20f));

                    if (choosePath)
                        m_nestedGraphPath = EditorUtility.SaveFilePanelInProject("Save asset", "nested", "asset", "Hallo");

                    GUILayout.Label($"Path: {m_nestedGraphPath}");
                }
            }
        }

        void NodesGUI()
        {
            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                using (new EditorGUILayout.HorizontalScope())
                {
                    m_nodesExpanded = GUILayout.Toggle(m_nodesExpanded, "", EditorStyles.foldout, GUILayout.Width(20f));
                    m_nodesExpanded = EditorGUILayout.Foldout(m_nodesExpanded, "Nodes: ", true, EditorStyles.boldLabel);
                }
                if (!m_nodesExpanded)
                    return;
                for (var i = 0; i < m_nodes.Length; i++)
                    NodeGUI(ref m_nodes[i]);
            }
        }

        void MappedPortsGUI()
        {
            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                using (new EditorGUILayout.HorizontalScope())
                {
                    m_mappedPortsExpanded =
                        GUILayout.Toggle(m_mappedPortsExpanded, "", EditorStyles.foldout, GUILayout.Width(20f));
                    m_mappedPortsExpanded = EditorGUILayout.Foldout(m_mappedPortsExpanded, "Mapped Ports: ", true,
                        EditorStyles.boldLabel);
                }

                if (!m_mappedPortsExpanded)
                    return;
                var deleteIdx = -1;
                for (var i = 0; i < m_nodePorts.Count; i++)
                {
                    var np = m_nodePorts[i];
                    MappedPortGUI(np.Template, out var delete);
                    if (delete)
                        deleteIdx = i;
                }

                if (deleteIdx == -1)
                    return;

                m_nodePorts.RemoveAt(deleteIdx);
            }
        }

        bool m_allCheckedPortsMapped;

        void OKCancelGUI()
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                // todo: only do this when things change
                ValidateAllCheckedPortsMapped();

                bool okPressed;
                using (new EditorGUI.DisabledGroupScope(!CreateNestedValid())) 
                    okPressed = GUILayout.Button("OK");

                var cancel = GUILayout.Button("cancel");

                if (okPressed) 
                    CreateNested(m_originalGraph, m_newNodeNameContainingGraph, m_nestedGraphPath);

                if (okPressed || cancel)
                    Close();
            }
        }

        bool CreateNestedValid()
        {
            if (!m_allCheckedPortsMapped)
                return false;

            // todo: improve check path and name valid
            if (m_nestedGraphPath.IsNullOrEmpty())
                return false;
            if (m_newNodeNameContainingGraph.IsNullOrEmpty())
                return false;
            return true;
        }

        void ValidateAllCheckedPortsMapped()
        {
            var checkedNodes = m_nodes.Where(n => n.Checked);
            var checkedPorts = checkedNodes.SelectMany(n => n.Ports).Where(p => p.Checked);
            m_allCheckedPortsMapped = checkedPorts.All(p => p.MapToPortTemplate != null);
        }

        void NodeGUI(ref NodeData node)
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    node.Checked = EditorGUILayout.Toggle("", node.Checked, GUILayout.Width(20f));
                    // keep selection up to date
                    if (check.changed)
                        m_selection = m_nodes.Where(n => n.Checked).Select(n => n.Node);
                }

                node.IsExpanded =
                    EditorGUILayout.Foldout(node.IsExpanded, node.Node.Name, true, EditorStyles.foldoutHeader);
            }

            if (!node.IsExpanded)
                return;

            using (new EditorGUI.DisabledGroupScope(!node.Checked))
            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Space(20f);
                using (new EditorGUILayout.VerticalScope())
                {
                    for (var i = 0; i < node.Ports.Length; i++)
                        NodesPortGUI(node.Node, ref node.Ports[i]);
                }
            }
        }

        void NodesPortGUI(INamed node, ref NodesPortData port)
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                var initialPort = port.PortID.GetNodePort();
                port.Checked = EditorGUILayout.Toggle("", port.Checked, GUILayout.Width(20f));
                EditorGUILayout.LabelField(port.PortID.FieldName, GUILayout.Width(150f));
                EditorGUILayout.LabelField(initialPort.ValueType.Name, GUILayout.Width(150f));
                EditorGUILayout.LabelField($"{initialPort.Direction}", GUILayout.Width(50f));

                var portOptions = m_nodePorts.Where(np => np.Template.Direction == initialPort.Direction
                                                          && np.Template.ValueType == initialPort.ValueType).Select(np => np.Template).ToArray();
                var portNames = portOptions.Select(np => np.FieldName).ToArray();

                var idx = Array.IndexOf(portOptions, port.MapToPortTemplate);
                var newIdx = EditorGUILayout.Popup(idx, portNames, GUILayout.Width(150f));
                if (newIdx != idx)
                    port.MapToPortTemplate = portOptions[newIdx];

                var add = GUILayout.Button("+", GUILayout.Width(20f));
                if (!add)
                    return;

                var uniqueName = GetUniqueName(node, initialPort);
                var mapTo = new NodePort(uniqueName, initialPort.ValueType, initialPort.Direction, initialPort.ConnectionType, initialPort.TypeConstraint, null);
                m_nodePorts.Add(new PortMapping() { Template = mapTo });
            }
        }

        void MappedPortGUI(NodePort port, out bool delete)
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                delete = GUILayout.Button("x", GUILayout.Width(20f));

                var refCount = m_nodes.SelectMany(n => n.Ports).Count(p => p.MapToPortTemplate == port);
                GUILayout.Label($"ref: {refCount}", GUILayout.Width(50f));

                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    var newName = EditorGUILayout.DelayedTextField(port.FieldName, GUILayout.Width(150f));
                    EditorGUILayout.LabelField(port.ValueType.Name, GUILayout.Width(150f));
                    EditorGUILayout.LabelField($"{port.Direction}", GUILayout.Width(50f));

                    if (!check.changed)
                        return;

                    if (m_nodePorts.FindIndex(p => string.Equals(newName, p.Template.FieldName)) != -1)
                        return;

                    port.FieldName = newName;
                }
            }
        }

        void CreateNested(INodeGraph originalGraph, string nestedNodeName, string path)
        {
            if(!CreateNestedValid())
                return;

            var nestedGraph = CreateInstance<NestedNodeGraph>();
            AssetDatabase.CreateAsset(nestedGraph, path);
            EditorUtility.SetDirty(nestedGraph);
            AssetDatabase.SaveAssets();

            var inputs = nestedGraph.AddNode<DynamicPortsNode>("Inputs");
            var outputs = nestedGraph.AddNode<DynamicPortsNode>("Outputs");
            var nestedGraphNode = originalGraph.AddNode<NestedNodeGraphNode>(nestedNodeName);

            for (var i = 0; i < m_nodePorts.Count; i++)
            {
                var mapping = m_nodePorts[i];
                var port = mapping.Template;
                var dir = ReverseDir(port.Direction);
                if (port.Direction == IO.Input)
                    mapping.InnerPort = inputs.AddDynamicPort(port.ValueType, dir, port.ConnectionType, port.TypeConstraint,
                        port.FieldName);
                else
                    mapping.InnerPort = outputs.AddDynamicPort(port.ValueType, dir, port.ConnectionType, port.TypeConstraint,
                        port.FieldName);

                mapping.OuterPort = nestedGraphNode.AddDynamicPort(port.ValueType, port.Direction, port.ConnectionType, port.TypeConstraint,
                    port.FieldName);

                m_nodePorts[i] = mapping;
            }

            using (var mappingScope = SimplePool<List<AssetExtraction.SubAssetMapping>>.I.GetScoped())
            {
                var mapping = mappingScope.Obj;
                foreach (var nd in m_nodes)
                {
                    var nodeHierarchy = SubAssetHierarchy.GetSubAssetHierarchy(nd.Node as Object);
                    AssetExtraction.ExtractSubSubAsset(nestedGraph, nodeHierarchy, mapping);
                }

                RemapConnections(originalGraph, nestedGraph, mapping);

                nestedGraphNode.SetGraph(nestedGraph);
                // save dynamic nodes for inputs and outputs in nested and original graph
                inputs.SaveDynamicNodes(nestedGraph.GetPorts(inputs).ToArray());
                outputs.SaveDynamicNodes(nestedGraph.GetPorts(outputs).ToArray());
                nestedGraphNode.SaveDynamicNodes(originalGraph.GetPorts(nestedGraphNode).ToArray());

                AssetDatabase.SaveAssets();
                AssetExtraction.ReplaceAllGuids(path, mapping);

                foreach (var entry in mapping)
                    entry.OrigAsset.DestroyEx();

                AssetDatabase.Refresh();
            }
        }

        #region Connection Mapping
        void RemapConnections(INodeGraph originalGraph, INodeGraph nestedGraph, IReadOnlyCollection<AssetExtraction.SubAssetMapping> subAssetMapping)
        {
            using (var removeConnectionsScoped = SimplePool<List<Connection>>.I.GetScoped())
            {
                foreach (var c in originalGraph.Connections)
                {
                    var result = RemapConnection(originalGraph, nestedGraph, subAssetMapping, c);
                    if (result == RemapConnectionResult.ConnectionAffected)
                        removeConnectionsScoped.Obj.Add(c);
                }

                originalGraph.RemoveConnections(removeConnectionsScoped.Obj);
            }
        }

        enum RemapConnectionResult
        {
            ConnectionIgnored,
            ConnectionAffected
        }

        RemapConnectionResult RemapConnection(INodeGraph originalGraph, INodeGraph nestedGraph, IReadOnlyCollection<AssetExtraction.SubAssetMapping> subAssetMapping, Connection c)
        {
            var fromTarget = c.From.Node;
            var toTarget = c.To.Node;

            var fromSelected = m_selection.Contains((INode)fromTarget);
            var toSelected = m_selection.Contains((INode)toTarget);
            var ignoreConnection = !(fromSelected || toSelected);
            var innerOnlyConnection = fromSelected && toSelected;

            if (ignoreConnection)
                return RemapConnectionResult.ConnectionIgnored;

            PortID fromInnerPortID = default;
            PortID toInnerPortID = default;
            PortID fromOuterPortID = default;
            PortID toOuterPortID = default;

            if (fromSelected)
                RemapPortIDsForConnectionFrom(subAssetMapping, c, innerOnlyConnection, out fromInnerPortID, ref toInnerPortID, ref fromOuterPortID, ref toOuterPortID);
            if (toSelected)
                RemapPortIDsForConnectionTo(subAssetMapping, c, innerOnlyConnection, ref fromInnerPortID, out toInnerPortID, ref fromOuterPortID, ref toOuterPortID);

            if (fromInnerPortID.Node != null && toInnerPortID.Node != null)
                nestedGraph.AddConnection(new Connection() { From = fromInnerPortID, To = toInnerPortID });
            if (!innerOnlyConnection && fromOuterPortID.Node != null && toOuterPortID.Node != null)
                originalGraph.AddConnection(new Connection() { From = fromOuterPortID, To = toOuterPortID });

            return RemapConnectionResult.ConnectionAffected;
        }

        void RemapPortIDsForConnectionFrom(IEnumerable<AssetExtraction.SubAssetMapping> subAssetMapping,
            Connection originalConnection, bool innerOnlyConnection,
            out PortID fromInnerPortID, ref PortID toInnerPortID, ref PortID fromOuterPortID, ref PortID toOuterPortID)
        {
            var originalFromNode = originalConnection.From.Node;
            var newInnerFromNode = (SO)subAssetMapping.FirstOrDefault(m => m.OrigAsset == originalFromNode).CloneAsset;
            fromInnerPortID = new PortID() { Node = newInnerFromNode, FieldName = originalConnection.From.FieldName };

            if (innerOnlyConnection)
                return;

            var fromData = m_nodes.FirstOrDefault(n => ReferenceEquals(n.Node, originalFromNode));
            Debug.Assert(fromData.Checked, $"Unexpected! Connection {originalConnection} connects from new nested node to outer node, " +
                                           $"but nested has not been checked");

            var fromPortData = fromData.Ports.FirstOrDefault(p => p.PortID == originalConnection.From);
            if (!fromPortData.Checked)
            {
                Debug.LogWarning($"Connection will be removed {originalConnection}, because Port {fromPortData.PortID} is not checked");
                return;
            }

            var portMapping = m_nodePorts.Find(pm => pm.Template == fromPortData.MapToPortTemplate);
            toInnerPortID = portMapping.InnerPort.ID;
            fromOuterPortID = portMapping.OuterPort.ID;
            toOuterPortID = originalConnection.To;
        }

        void RemapPortIDsForConnectionTo(IEnumerable<AssetExtraction.SubAssetMapping> subAssetMapping,
            Connection originalConnection, bool innerOnlyConnection,
            ref PortID fromInnerPortID, out PortID toInnerPortID, ref PortID fromOuterPortID, ref PortID toOuterPortID)
        {
            var originalToNode = originalConnection.To.Node;
            var newInnerToNode = (SO)subAssetMapping.FirstOrDefault(m => m.OrigAsset == originalToNode).CloneAsset;
            toInnerPortID = new PortID() { Node = newInnerToNode, FieldName = originalConnection.To.FieldName };

            if (innerOnlyConnection)
                return;

            var toData = m_nodes.FirstOrDefault(n => ReferenceEquals(n.Node, originalToNode));
            Debug.Assert(toData.Checked, $"Unexpected! Connection {originalConnection} connects to new nested node from outer node, " +
                                         $"but nested has not been checked");

            var toPortData = toData.Ports.FirstOrDefault(p => p.PortID == originalConnection.To);
            if (!toPortData.Checked)
            {
                Debug.LogWarning($"Connection will be removed {originalConnection}, because Port {toPortData.PortID} is not checked");
                return;
            }

            var portMapping = m_nodePorts.Find(pm => pm.Template == toPortData.MapToPortTemplate);
            fromInnerPortID = portMapping.InnerPort.ID;
            toOuterPortID = portMapping.OuterPort.ID;
            fromOuterPortID = originalConnection.From;
        }

        static IO ReverseDir(IO dir) =>
            dir == IO.Input
                ? IO.Output
                : IO.Input;
        #endregion
    }
}
