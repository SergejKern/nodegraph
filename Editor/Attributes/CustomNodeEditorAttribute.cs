using System;
using nodegraph.Editor.Interface;

namespace nodegraph.Editor.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class CustomNodeEditorAttribute : Attribute, INodeEditorAttribute
    {
        readonly Type m_inspectedType;

        /// <summary> Tells a NodeEditor which Node type it is an editor for </summary>
        /// <param name="inspectedType">Type that this editor can edit</param>
        public CustomNodeEditorAttribute(Type inspectedType)
            => m_inspectedType = inspectedType;

        public Type GetInspectedType() 
            => m_inspectedType;
    }
}