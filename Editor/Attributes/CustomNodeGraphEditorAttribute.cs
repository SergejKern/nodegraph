﻿using System;
using nodegraph.Editor.Interface;

namespace nodegraph.Editor.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class CustomNodeGraphEditorAttribute : Attribute, INodeEditorAttribute
    {
        readonly Type m_inspectedType;
        public readonly string EditorPrefsKey;

        /// <summary> Tells a NodeGraphEditor which Graph type it is an editor for </summary>
        /// <param name="inspectedType">Type that this editor can edit</param>
        /// <param name="editorPrefsKey">Define unique key for unique layout settings instance</param>
        public CustomNodeGraphEditorAttribute(Type inspectedType, string editorPrefsKey = "nodegraph.Settings")
        {
            m_inspectedType = inspectedType;
            EditorPrefsKey = editorPrefsKey;
        }

        public Type GetInspectedType() => m_inspectedType;
    }
}