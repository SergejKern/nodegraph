﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using nodegraph.Editor.Interface;
using static Core.Editor.Extensions.EditorExtensions;

namespace nodegraph.Editor.Utility
{
    public static class NodeEditorCache
    {
        public static readonly Dictionary<NodePort, Vector2> PortPositions = new Dictionary<NodePort, Vector2>();

        static readonly Dictionary<ScriptableObject, INodeEditorBase> k_editors = new Dictionary<ScriptableObject, INodeEditorBase>();
        static Dictionary<Type, Type> m_editorTypes;

        public static INodeEditor GetEditor(INode target, NodeEditorWindow window)
            => (INodeEditor) GetEditor((ScriptableObject) target, window);
        
        public static INodeGraphEditor GetEditor(INodeGraph target, NodeEditorWindow window)
            => (INodeGraphEditor) GetEditor((ScriptableObject) target, window);

        public static INodeEditorBase GetEditor(ScriptableObject target, NodeEditorWindow window)
        {
            if (target == null) 
                return null;

            if (k_editors.TryGetValue(target, out var editor))
                editor.SetTargetData(window, target);
            else
            {
                var type = target.GetType();
                var editorType = GetEditorType(type);
                //Debug.Log(editorType);
                editor = (INodeEditorBase) Activator.CreateInstance(editorType);
                editor.Init(window, target);
                k_editors.Add(target, editor);
            }

            return editor;
        }

        static Type GetEditorType(Type type)
        {
            var originalType = type;

            while (true)
            {
                if (type == null) 
                    return GetEditorTypeByInterface(originalType);
                if (m_editorTypes == null) 
                    CacheCustomEditors();
                Debug.Assert(m_editorTypes != null, nameof(m_editorTypes) + " != null");
                if (m_editorTypes.TryGetValue(type, out var result)) 
                    return result;

                type = type.BaseType;
                //If type isn't found, try base type
            }
        }

        static Type GetEditorTypeByInterface(Type type)
        {
            var editor = m_editorTypes.Where(edType 
                    => edType.Key.IsInterface && edType.Key.IsAssignableFrom(type))
                .OrderBy(e => e.Key.GetInterfaces().Length).Last();
            return editor.Value;
        }

        static void CacheCustomEditors()
        {
            m_editorTypes = new Dictionary<Type, Type>();

            var types = new List<Type>();
            ReflectionGetAllTypes(typeof(INodeEditorBase), ref types, out var names, out var namespaces);
            //Debug.Log($"reflection found {types.Count} editors");
            foreach (var editorType in types)
            {
                if (editorType.IsAbstract || editorType.IsInterface) 
                    continue;

                var attributes = editorType.GetCustomAttributes(typeof(INodeEditorAttribute), false);
                //Debug.Log($"editorType {editorType} attributes {attributes.Length}");

                if (attributes.Length == 0) 
                    continue;
                if (!(attributes[0] is INodeEditorAttribute attribute))
                    throw new InvalidOperationException($"Missing {nameof(INodeEditorAttribute)} on {editorType.Name}");

                var inspectedType = attribute.GetInspectedType();
                var inspectedIsNode = typeof(INode).IsAssignableFrom(inspectedType);
                var inspectedIsGraph = typeof(INodeGraph).IsAssignableFrom(inspectedType);
                var editorIsNodeEditor = typeof(INodeEditor).IsAssignableFrom(editorType);
                var editorIsGraphEditor = typeof(INodeGraphEditor).IsAssignableFrom(editorType);

                if (inspectedIsNode && !editorIsNodeEditor)
                    throw new InvalidOperationException($"{editorType.Name} inspects {nameof(INode)}, but must also implement {nameof(INodeEditor)}");
                if (inspectedIsGraph && !editorIsGraphEditor)
                    throw new InvalidOperationException($"{editorType.Name} inspects {nameof(INodeGraph)}, but must also implement {nameof(INodeGraphEditor)}");

                //Debug.Log($"editorType {editorType} inspected: {attribute.GetInspectedType()}");

                m_editorTypes.Add(attribute.GetInspectedType() ?? throw new InvalidOperationException(), editorType);
            }
        }
    }
}