﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using nodegraph.Attributes;

using Object = UnityEngine.Object;
using ContextMethodPair = System.Collections.Generic.KeyValuePair<UnityEngine.ContextMenu, System.Reflection.MethodInfo>;

namespace nodegraph.Editor
{
    /// <summary> Contains reflection-related extensions built for xNode </summary>
    public static class NodeEditorReflection
    {
        static Dictionary<Type, Color> m_nodeTint;
        static Dictionary<Type, int> m_nodeWidth;

        /// <summary> All available node types </summary>
        public static IEnumerable<Type> NodeTypes => m_nodeTypes ?? (m_nodeTypes = GetNodeTypes());
        static Type[] m_nodeTypes;

        /// <summary> Return a delegate used to determine whether window is docked or not. It is faster to cache this delegate than run the reflection required each time. </summary>
        public static Func<bool> GetIsDockedDelegate(this EditorWindow window)
        {
            const BindingFlags fullBinding = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;
            
            var isDockedMethod = typeof(EditorWindow).GetProperty("docked", fullBinding)?.GetGetMethod(true);
            return (Func<bool>)Delegate.CreateDelegate(typeof(Func<bool>), window, isDockedMethod ?? throw new InvalidOperationException());
        }

        //Get all classes deriving from Node via reflection
        static Type[] GetNodeTypes() => GetDerivedTypes(typeof(INodeData));

        /// <summary> Custom node tint colors defined with [NodeColor(r, g, b)] </summary>
        public static bool TryGetAttributeTint(this Type nodeType, out Color tint)
        {
            if (m_nodeTint == null) 
                CacheAttributes<Color, NodeTintAttribute>(out m_nodeTint, x => x.Color);
            
            return m_nodeTint.TryGetValue(nodeType, out tint);
        }

        /// <summary> Get custom node widths defined with [NodeWidth(width)] </summary>
        public static bool TryGetAttributeWidth(this Type nodeType, out int width)
        {
            if (m_nodeWidth == null) 
                CacheAttributes<int, NodeWidthAttribute>(out m_nodeWidth, x => x.Width);
            
            return m_nodeWidth.TryGetValue(nodeType, out width);
        }

        static void CacheAttributes<TValue, TAttribute>(out Dictionary<Type, TValue> dict, Func<TAttribute, TValue> getter) where TAttribute : Attribute
        {
            dict = new Dictionary<Type, TValue>();
            foreach (var nodeType in NodeTypes)
            {
                var attributes = nodeType.GetCustomAttributes(typeof(TAttribute), true);
                if (attributes.Length == 0) 
                    continue;
                var attribute = attributes[0] as TAttribute;
                dict.Add(nodeType, getter(attribute));
            }
        }

        /// <summary> Get FieldInfo of a field, including those that are private and/or inherited </summary>
        public static FieldInfo GetFieldInfo(this Type type, string fieldName)
        {
            // If we can't find field in the first run, it's probably a private field in a base class.
            var field = type.GetField(fieldName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            // Search base classes for private fields only. Public fields are found above
            while (field == null 
                   && (type = type?.BaseType) != typeof(Node)) 
                field = type?.GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance);
            return field;
        }

        /// <summary> Get all classes deriving from baseType via reflection </summary>
        public static Type[] GetDerivedTypes(this Type baseType)
        {
            var types = new List<Type>();
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies)
            {
                try
                {
                    types.AddRange(assembly.GetTypes().Where(t => !t.IsAbstract && baseType.IsAssignableFrom(t)).ToArray());
                }
                catch (ReflectionTypeLoadException) { }
            }
            return types.ToArray();
        }

        /// <summary> Find methods marked with the [ContextMenu] attribute and add them to the context menu </summary>
        public static void AddCustomContextMenuItems(this GenericMenu contextMenu, object obj)
        {
            var items = GetContextMenuMethods(obj);
            if (items.Length == 0) 
                return;

            contextMenu.AddSeparator("");
            // foreach (var checkValidate in items)if (checkValidate.Key.validate &&_!(bool)checkValidate.Value.Invoke(obj, null))invalidatedEntries.Add(checkValidate.Key.menuItem);
            var invalidatedEntries = (from checkValidate in items 
                where checkValidate.Key.validate 
                      && !(bool) checkValidate.Value.Invoke(obj, null) 
                select checkValidate.Key.menuItem).ToList();

            foreach (var kvp in items)
            {
                if (invalidatedEntries.Contains(kvp.Key.menuItem))
                    contextMenu.AddDisabledItem(new GUIContent(kvp.Key.menuItem));
                else
                {
                    var kvp1 = kvp;
                    contextMenu.AddItem(new GUIContent(kvp.Key.menuItem), false, () => kvp1.Value.Invoke(obj, null));
                }
            }
        }

        /// <summary> Call OnValidate on target </summary>
        public static void TriggerOnValidate(this Object target)
        {
            if (target == null) 
                return;
            var onValidate = target.GetType().GetMethod("OnValidate", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            if (onValidate != null) 
                onValidate.Invoke(target, null);
        }

        static ContextMethodPair[] GetContextMenuMethods(object obj)
        {
            var type = obj.GetType();
            var methods = type.GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
            var kvp = new List<ContextMethodPair>();
            foreach (var methodInfo in methods)
            {
                var attributes = methodInfo.GetCustomAttributes(typeof(ContextMenu), true)
                    .Select(x => x as ContextMenu).ToArray();

                if (attributes.Length == 0) 
                    continue;
                if (methodInfo.GetParameters().Length != 0)
                {
                    Debug.LogWarning($"Method {methodInfo.DeclaringType?.Name}.{methodInfo.Name} has parameters and cannot be used for context menu commands.");
                    continue;
                }
                if (methodInfo.IsStatic)
                {
                    Debug.LogWarning($"Method {methodInfo.DeclaringType?.Name}.{methodInfo.Name} is static and cannot be used for context menu commands.");
                    continue;
                }

                kvp.AddRange(attributes.Select(t => new ContextMethodPair(t, methodInfo)));
            }
            //Sort menu items
            kvp.Sort((x, y) => x.Key.priority.CompareTo(y.Key.priority));
            return kvp.ToArray();
        }

        /// <summary> Very crude. Uses a lot of reflection. </summary>
        public static void OpenPreferences()
        {
            try
            {
                SettingsService.OpenUserPreferences("Preferences/Node Editor");
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                Debug.LogWarning("Unity has changed around internally. Can't open properties through reflection. Please contact xNode developer and supply unity version number.");
            }
        }
    }
}
