﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using nodegraph.Editor.Enums;

namespace nodegraph.Editor.Utility
{
    public static class NoodleGUI
    {
        static readonly Vector3[] k_polyLineTempArray = new Vector3[2];

        /// <summary> Draws a line segment without allocating temporary arrays </summary>
        static void DrawAAPolyLineNonAlloc(float thickness, Vector2 p0, Vector2 p1)
        {
            k_polyLineTempArray[0].x = p0.x;
            k_polyLineTempArray[0].y = p0.y;
            k_polyLineTempArray[1].x = p1.x;
            k_polyLineTempArray[1].y = p1.y;
            Handles.DrawAAPolyLine(thickness, k_polyLineTempArray);
        }

        public static void DrawNoodle(Gradient gradient,
            NoodlePath path, NoodleStroke stroke, float thickness,
            IList<Vector2> windowPoints, float zoom)
        {
            var originalHandlesColor = Handles.color;
            Handles.color = gradient.Evaluate(0f);
            var length = windowPoints.Count;

            switch (path)
            {
                case NoodlePath.Curvy:
                    DrawCurvyNoodle(gradient, stroke, thickness, windowPoints, length, zoom);
                    break;
                case NoodlePath.Straight:
                    DrawStraightNoodle(gradient, stroke, thickness, windowPoints, length);
                    break;
                case NoodlePath.Angled:
                    DrawAngledNoodle(gradient, thickness, windowPoints, length, zoom);
                    break;
                case NoodlePath.ShaderLab:
                    DrawShaderLabNoodle(gradient, stroke, thickness, windowPoints, length, zoom);
                    break;
                default: throw new ArgumentOutOfRangeException(nameof(path), path, null);
            }
            Handles.color = originalHandlesColor;
        }
        static void DrawShaderLabNoodle(Gradient gradient, NoodleStroke stroke, float thickness, IList<Vector2> gridPoints, int length, float zoom)
        {
            var start = gridPoints[0];
            var end = gridPoints[length - 1];
            //Modify first and last point in array so we can loop trough them nicely.
            gridPoints[0] = gridPoints[0] + Vector2.right * (20 / zoom);
            gridPoints[length - 1] = gridPoints[length - 1] + Vector2.left * (20 / zoom);
            //Draw first vertical lines going out from nodes
            Handles.color = gradient.Evaluate(0f);
            DrawAAPolyLineNonAlloc(thickness, start, gridPoints[0]);
            Handles.color = gradient.Evaluate(1f);
            DrawAAPolyLineNonAlloc(thickness, end, gridPoints[length - 1]);
            for (var i = 0; i < length - 1; i++)
            {
                var point_a = gridPoints[i];
                var point_b = gridPoints[i + 1];
                // Draws the line with the coloring.
                var prev_point = point_a;
                // Approximately one segment per 5 pixels
                var segments = (int)Vector2.Distance(point_a, point_b) / 5;
                segments = Math.Max(segments, 1);

                var draw = 0;
                for (var j = 0; j <= segments; j++)
                {
                    draw++;
                    var t = j / (float)segments;
                    var lerp = Vector2.Lerp(point_a, point_b, t);
                    if (draw > 0)
                    {
                        if (i == length - 2) Handles.color = gradient.Evaluate(t);
                        DrawAAPolyLineNonAlloc(thickness, prev_point, lerp);
                    }

                    prev_point = lerp;
                    if (stroke == NoodleStroke.Dashed && draw >= 2)
                        draw = -2;
                }
            }

            gridPoints[0] = start;
            gridPoints[length - 1] = end;
        }

        static void DrawAngledNoodle(Gradient gradient, float thickness, IList<Vector2> gridPoints, int length, float zoom)
        {
            for (var i = 0; i < length - 1; i++)
            {
                if (i == length - 1)
                    continue; // Skip last index
                if (gridPoints[i].x <= gridPoints[i + 1].x - (50 / zoom))
                {
                    var midpoint = (gridPoints[i].x + gridPoints[i + 1].x) * 0.5f;
                    var start_1 = gridPoints[i];
                    var end_1 = gridPoints[i + 1];
                    start_1.x = midpoint;
                    end_1.x = midpoint;
                    if (i == length - 2)
                    {
                        DrawAAPolyLineNonAlloc(thickness, gridPoints[i], start_1);
                        Handles.color = gradient.Evaluate(0.5f);
                        DrawAAPolyLineNonAlloc(thickness, start_1, end_1);
                        Handles.color = gradient.Evaluate(1f);
                        DrawAAPolyLineNonAlloc(thickness, end_1, gridPoints[i + 1]);
                    }
                    else
                    {
                        DrawAAPolyLineNonAlloc(thickness, gridPoints[i], start_1);
                        DrawAAPolyLineNonAlloc(thickness, start_1, end_1);
                        DrawAAPolyLineNonAlloc(thickness, end_1, gridPoints[i + 1]);
                    }
                }
                else
                {
                    var midpoint = (gridPoints[i].y + gridPoints[i + 1].y) * 0.5f;
                    var start_1 = gridPoints[i];
                    var end_1 = gridPoints[i + 1];
                    start_1.x += 25 / zoom;
                    end_1.x -= 25 / zoom;
                    var start_2 = start_1;
                    var end_2 = end_1;
                    start_2.y = midpoint;
                    end_2.y = midpoint;
                    if (i == length - 2)
                    {
                        DrawAAPolyLineNonAlloc(thickness, gridPoints[i], start_1);
                        Handles.color = gradient.Evaluate(0.25f);
                        DrawAAPolyLineNonAlloc(thickness, start_1, start_2);
                        Handles.color = gradient.Evaluate(0.5f);
                        DrawAAPolyLineNonAlloc(thickness, start_2, end_2);
                        Handles.color = gradient.Evaluate(0.75f);
                        DrawAAPolyLineNonAlloc(thickness, end_2, end_1);
                        Handles.color = gradient.Evaluate(1f);
                        DrawAAPolyLineNonAlloc(thickness, end_1, gridPoints[i + 1]);
                    }
                    else
                    {
                        DrawAAPolyLineNonAlloc(thickness, gridPoints[i], start_1);
                        DrawAAPolyLineNonAlloc(thickness, start_1, start_2);
                        DrawAAPolyLineNonAlloc(thickness, start_2, end_2);
                        DrawAAPolyLineNonAlloc(thickness, end_2, end_1);
                        DrawAAPolyLineNonAlloc(thickness, end_1, gridPoints[i + 1]);
                    }
                }
            }
        }

        static void DrawStraightNoodle(Gradient gradient, NoodleStroke stroke, float thickness, IList<Vector2> gridPoints, int length)
        {
            for (var i = 0; i < length - 1; i++)
            {
                var point_a = gridPoints[i];
                var point_b = gridPoints[i + 1];
                // Draws the line with the coloring.
                var prev_point = point_a;
                // Approximately one segment per 5 pixels
                var segments = (int)Vector2.Distance(point_a, point_b) / 5;
                segments = Math.Max(segments, 1);

                var draw = 0;
                for (var j = 0; j <= segments; j++)
                {
                    draw++;
                    var t = j / (float)segments;
                    var lerp = Vector2.Lerp(point_a, point_b, t);
                    if (draw > 0)
                    {
                        if (i == length - 2)
                            Handles.color = gradient.Evaluate(t);
                        DrawAAPolyLineNonAlloc(thickness, prev_point, lerp);
                    }

                    prev_point = lerp;
                    if (stroke == NoodleStroke.Dashed && draw >= 2)
                        draw = -2;
                }
            }
        }

        static void DrawCurvyNoodle(Gradient gradient, NoodleStroke stroke, float thickness, IList<Vector2> gridPoints, int length, float zoom)
        {
            var outputTangent = Vector2.right;
            for (var i = 0; i < length - 1; i++)
            {
                Vector2 inputTangent;
                // Cached most variables that repeat themselves here to avoid so many indexer calls :p
                var point_a = gridPoints[i];
                var point_b = gridPoints[i + 1];
                var dist_ab = Vector2.Distance(point_a, point_b);
                if (i == 0) outputTangent = zoom * dist_ab * 0.01f * Vector2.right;
                if (i >= length - 2)
                    inputTangent = zoom * dist_ab * 0.01f * Vector2.left;
                else
                {
                    var point_c = gridPoints[i + 2];
                    var ab = (point_b - point_a).normalized;
                    var cb = (point_b - point_c).normalized;
                    var ac = (point_c - point_a).normalized;
                    var p = (ab + cb) * 0.5f;
                    var tangentLength = (dist_ab + Vector2.Distance(point_b, point_c)) * 0.005f * zoom;
                    var side = ((ac.x * (point_b.y - point_a.y)) - (ac.y * (point_b.x - point_a.x)));

                    p = tangentLength * Mathf.Sign(side) * new Vector2(-p.y, p.x);
                    inputTangent = p;
                }

                // Calculates the tangents for the bezier curves.
                var zoomCoefficient = 50 / zoom;
                var tangent_a = point_a + outputTangent * zoomCoefficient;
                var tangent_b = point_b + inputTangent * zoomCoefficient;
                // Hover effect.
                var division = Mathf.RoundToInt(.2f * dist_ab) + 3;
                // Coloring and bezier drawing.
                var draw = 0;
                var bezierPrevious = point_a;
                for (var j = 1; j <= division; ++j)
                {
                    if (stroke == NoodleStroke.Dashed)
                    {
                        draw++;
                        if (draw >= 2)
                            draw = -2;
                        if (draw < 0)
                            continue;
                        if (draw == 0)
                            bezierPrevious = CalculateBezierPoint(point_a, tangent_a, tangent_b, point_b, (j - 1f) / division);
                    }

                    if (i == length - 2)
                        Handles.color = gradient.Evaluate((j + 1f) / division);
                    var bezierNext = CalculateBezierPoint(point_a, tangent_a, tangent_b, point_b, j / (float)division);
                    DrawAAPolyLineNonAlloc(thickness, bezierPrevious, bezierNext);
                    bezierPrevious = bezierNext;
                }

                outputTangent = -inputTangent;
            }
        }

        static Vector2 CalculateBezierPoint(Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3, float t)
        {
            var u = 1 - t;
            float tt = t * t, uu = u * u;
            float uuu = uu * u, ttt = tt * t;
            return new Vector2(
                (uuu * p0.x) + (3 * uu * t * p1.x) + (3 * u * tt * p2.x) + (ttt * p3.x),
                (uuu * p0.y) + (3 * uu * t * p1.y) + (3 * u * tt * p2.y) + (ttt * p3.y)
            );
        }
    }
}