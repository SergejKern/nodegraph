﻿using System;
using UnityEngine;

namespace nodegraph.Editor.Utility
{
    public struct ZoomScope : IDisposable
    {
        Rect m_rect;
        readonly float m_zoom;
        readonly float m_topPadding;

        public ZoomScope(Rect rect, float zoom, float topPadding)
        {
            m_rect = rect;
            m_zoom = zoom;
            m_topPadding = topPadding;

            GUI.EndClip();

            GUIUtility.ScaleAroundPivot(Vector2.one / zoom, rect.size * 0.5f);
            // var padding = new Vector4(0, topPadding, 0, 0);
            // padding *= zoom;
            GUI.BeginClip(new Rect(-((rect.width * zoom) - rect.width) * 0.5f, -(((rect.height * zoom) - rect.height) * 0.5f) + (topPadding * zoom),
                rect.width * zoom,
                rect.height * zoom));
        }

        public void Dispose()
        {
            GUIUtility.ScaleAroundPivot(Vector2.one * m_zoom, m_rect.size * 0.5f);
            var offset = new Vector3(
                (((m_rect.width * m_zoom) - m_rect.width) * 0.5f),
                (((m_rect.height * m_zoom) - m_rect.height) * 0.5f) + (-m_topPadding * m_zoom) + m_topPadding,
                0);
            GUI.matrix = Matrix4x4.TRS(offset, Quaternion.identity, Vector3.one);
        }
    }
}