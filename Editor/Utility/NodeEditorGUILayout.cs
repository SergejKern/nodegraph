﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Types;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using nodegraph.Attributes;
using nodegraph.Editor.Cache;
using nodegraph.Editor.Resources;
using nodegraph.Editor.Utility;
using nodegraph.Interface;
using ReorderableDict = System.Collections.Generic.Dictionary<string, UnityEditorInternal.ReorderableList>;

using ObjectToReorderableDict = System.Collections.Generic.Dictionary<UnityEngine.Object,
    System.Collections.Generic.Dictionary<string, UnityEditorInternal.ReorderableList>>;

using nodegraph.Operations;
using nodegraph.Editor.Operations;

using Object = UnityEngine.Object;

namespace nodegraph.Editor
{
    /// <summary> xNode-specific version of <see cref="EditorGUILayout"/> </summary>
    public static class NodeEditorGUILayout
    {
        static readonly ObjectToReorderableDict k_reorderableListCache = new ObjectToReorderableDict();

        const int k_portSize = 16;
        const int k_minWidth = 30;

        #region PropertyField
        #region public
        /// <summary> Make a field for a serialized property. Automatically displays relevant node port. </summary>
        public static void PropertyField(SerializedProperty property, bool includeChildren = true, bool drawOnlyWithPort = false) =>
            PropertyField(property, (GUIContent)null, includeChildren, drawOnlyWithPort);
        /// <summary> Make a field for a serialized property. Manual node port override. </summary>
        public static void PropertyField(SerializedProperty property, NodePort port, bool includeChildren = true, bool drawOnlyWithPort = false)
            => PropertyField(property, null, port, includeChildren, drawOnlyWithPort);
        #endregion

        static INode GetNode(SerializedProperty property)
        {
            var target = property.serializedObject.targetObject;
            switch (target)
            {
                case INode n: return n;
                case INodeData d: return d.Node;
            }

            return null;
        }

        /// <summary> Make a field for a serialized property. Automatically displays relevant node port. </summary>
        static void PropertyField(SerializedProperty property, GUIContent label, bool includeChildren = true, bool drawOnlyWithPort = false)
        {
            if (property == null)
                throw new NullReferenceException();
            var node = GetNode(property);
            var port = node.GetPort(property.name);
            PropertyField(property, label, port, includeChildren, drawOnlyWithPort);
        }

        /// <summary> Make a field for a serialized property. Manual node port override. </summary>
        static void PropertyField(SerializedProperty property, GUIContent label, NodePort port,
            bool includeChildren = true, bool drawOnlyWithPort = false)
        {
            if (property == null)
                throw new NullReferenceException();

            // If property is not a port, display a regular property field
            if (DrawRegularWithoutPort(property, label, port, includeChildren, drawOnlyWithPort) == FlowResult.Stop)
                return;
            if (port.IsDynamic)
                return;

            Rect rect;
            var propertyAttributes = NodeEditorUtilities.GetCachedPropertyAttributes(port.Node.NodeData.GetType(), property.name);

            switch (port.Direction)
            {
                // If property is an input, display a regular property field and put a port handle on the left side
                case IO.Input:
                    {
                        if (DrawInputPropertyField(property, label, port, includeChildren, propertyAttributes, out rect) == FlowResult.Stop)
                            return;
                        break;
                    }
                case IO.Output:
                    {
                        if (DrawOutputPropertyField(property, label, port, includeChildren, propertyAttributes, out rect) == FlowResult.Stop)
                            return;
                        break;
                    }
                default: throw new ArgumentOutOfRangeException();
            }

            rect.size = new Vector2(k_portSize, k_portSize);

            DrawPortHandle(port, rect);
            RegisterHandlePosition(port, rect);
        }
        #endregion

        #region Draw Methods
        static FlowResult DrawRegularWithoutPort(SerializedProperty property, GUIContent label, NodePort port, bool includeChildren,
            bool drawOnlyWithPort)
        {
            if (port != null)
                return FlowResult.Continue;
            if (drawOnlyWithPort)
                return FlowResult.Stop;
            EditorGUILayout.PropertyField(property, label, includeChildren, GUILayout.MinWidth(30));
            return FlowResult.Stop;
        }

        static FlowResult DrawPropertyField<T>(SerializedProperty property, LabelData labelData, NodePort port, bool includeChildren,
            IEnumerable<PropertyAttribute> propertyAttributes, ConnectionType defaultConnectionType, out float spacePadding) where T : Attribute, IInputOutputAttribute
        {
            GetAttributeData<T>(property, port, out var attr);
            ApplyPropertyAttributes(propertyAttributes, attr.UsePropertyAttributes, out var tooltip, out spacePadding);

            if (DrawDynamicPortList(property, port, attr.DynamicPortList, attr.Attribute, defaultConnectionType) == FlowResult.Stop)
                return FlowResult.Stop;

            DrawPropertyField(property, port, includeChildren, attr.ShowBacking, tooltip, labelData);

            return FlowResult.Continue;
        }

        static FlowResult DrawInputPropertyField(SerializedProperty property, GUIContent label, NodePort port, bool includeChildren,
            IEnumerable<PropertyAttribute> propertyAttributes, out Rect rect)
        {
            rect = default;

            var labelData = new LabelData()
            {
                Label = label,
                LabelStyle = EditorStyles.label,
                MinWidth = 0
            };
            if (DrawPropertyField<InputAttribute>(property, labelData, port, includeChildren, propertyAttributes,
                InputAttribute.DefaultConnectionType, out var spacePadding) == FlowResult.Stop)
                return FlowResult.Stop;

            // offset for input properties
            rect = GUILayoutUtility.GetLastRect();
            rect.position -= new Vector2(k_portSize, -spacePadding);
            // If property is an output, display a text label and put a port handle on the right side
            return FlowResult.Continue;
        }

        static FlowResult DrawOutputPropertyField(SerializedProperty property,
            GUIContent label, NodePort port, bool includeChildren,
            IEnumerable<PropertyAttribute> propertyAttributes, out Rect rect)
        {
            rect = default;

            var labelData = new LabelData()
            {
                Label = label,
                LabelStyle = NodeEditorResources.OutputPort,
                MinWidth = k_minWidth
            };
            if (DrawPropertyField<OutputAttribute>(property, labelData, port, includeChildren, propertyAttributes,
                OutputAttribute.DefaultConnectionType, out var spacePadding) == FlowResult.Stop)
                return FlowResult.Stop;

            // offset for output properties
            rect = GUILayoutUtility.GetLastRect();
            rect.position += new Vector2(rect.width, spacePadding);

            return FlowResult.Continue;
        }

        static void DrawPropertyField(SerializedProperty property, NodePort port, bool includeChildren,
            ShowBackingValue showBacking, string tooltip, LabelData label)
        {
            switch (showBacking)
            {
                case ShowBackingValue.Unconnected:
                    // Display a label if port is connected
                    if (port.IsConnected)
                        EditorGUILayout.LabelField(label.Label ?? new GUIContent(property.displayName, tooltip), label.LabelStyle,
                            GUILayout.MinWidth(label.MinWidth));
                    // Display an editable property field if port is not connected
                    else
                        EditorGUILayout.PropertyField(property, label.Label, includeChildren, GUILayout.MinWidth(k_minWidth));
                    break;
                case ShowBackingValue.Never:
                    // Display a label
                    EditorGUILayout.LabelField(label.Label ?? new GUIContent(property.displayName, tooltip),
                        label.LabelStyle, GUILayout.MinWidth(label.MinWidth));
                    break;
                case ShowBackingValue.Always:
                    // Display an editable property field
                    EditorGUILayout.PropertyField(property, label.Label, includeChildren, GUILayout.MinWidth(k_minWidth));
                    break;
                default: throw new ArgumentOutOfRangeException();
            }
        }

        static void DrawPortHandle(NodePort port, Rect rect)
        {
            var editor = NodeEditorCache.GetEditor(port.Node, NodeEditorWindow.Current);
            var backgroundColor = editor.GetTint();
            var col = NodeEditorWindow.Current.GraphEditor.GetPortColor(port);
            DrawPortHandle(rect, backgroundColor, col);
        }

        public static void DrawPortHandle(Rect rect, Color backgroundColor, Color typeColor)
        {
            var col = GUI.color;
            GUI.color = backgroundColor;
            GUI.DrawTexture(rect, NodeEditorResources.DotOuter);
            GUI.color = typeColor;
            GUI.DrawTexture(rect, NodeEditorResources.Dot);
            GUI.color = col;
        }

        static FlowResult DrawDynamicPortList(SerializedProperty property, NodePort port, bool dynamicPortList,
            IInputOutputAttribute attr, ConnectionType defaultConnectionType)
        {
            if (!dynamicPortList)
                return FlowResult.Continue;

            var type = GetType(property);
            var connectionType = attr?.ConnectionType ?? defaultConnectionType;
            DrawDynamicPortList(property.name, type, property.serializedObject, port.Direction, connectionType);
            return FlowResult.Stop;
        }

        /// <summary> Draw an editable list of dynamic ports. Port names are named as "[fieldName] [index]" </summary>
        /// <param name="fieldName">Supply a list for editable values</param>
        /// <param name="type">Value type of added dynamic ports</param>
        /// <param name="serializedObject">The serializedObject of the node</param>
        /// <param name="io">input or output</param>
        /// <param name="connectionType">Connection type of added dynamic ports</param>
        /// <param name="typeConstraint">constraint</param>
        /// <param name="onCreation">Called on the list on creation. Use this if you want to customize the created ReorderableList</param>
        static void DrawDynamicPortList(string fieldName, Type type, SerializedObject serializedObject,
            IO io, ConnectionType connectionType = ConnectionType.Multiple, TypeConstraint typeConstraint = TypeConstraint.None,
            Action<ReorderableList> onCreation = null)
        {
            var node = (INode)serializedObject.targetObject;

            var indexedPorts = node.DynamicPorts().Select(x =>
            {
                var split = x.FieldName.Split(' ');
                if (split.Length != 2 || split[0] != fieldName)
                    return new { index = -1, port = (NodePort)null };

                return int.TryParse(split[1], out var i)
                    ? new { index = i, port = x }
                    : new { index = -1, port = (NodePort)null };
            }).Where(x => x.port != null);
            var dynamicPorts = indexedPorts.OrderBy(x => x.index).Select(x => x.port).ToList();

            NodeDataCache.I.UpdatePorts(node);

            ReorderableList list = null;
            if (k_reorderableListCache.TryGetValue(serializedObject.targetObject, out var rlc))
                rlc.TryGetValue(fieldName, out list);
            // If a ReorderableList isn't cached for this array, do so.
            if (list == null)
            {
                var arrayData = serializedObject.FindProperty(fieldName);
                list = new ReorderablePortList(dynamicPorts, fieldName, arrayData, type, serializedObject, 
                    io, connectionType, typeConstraint);
                onCreation?.Invoke(list);
                    
                if (k_reorderableListCache.TryGetValue(serializedObject.targetObject, out rlc))
                    rlc.Add(fieldName, list);
                else
                    k_reorderableListCache.Add(serializedObject.targetObject, new ReorderableDict() { { fieldName, list } });
            }
            list.list = dynamicPorts;
            list.DoLayoutList();
        }
        #endregion

        #region Custom Data structs
        struct AttributeData
        {
            public ShowBackingValue ShowBacking;
            public bool DynamicPortList;
            public IInputOutputAttribute Attribute;
            public bool UsePropertyAttributes;
        }

        struct LabelData
        {
            public GUIContent Label;
            public GUIStyle LabelStyle;
            public int MinWidth;
        }
        #endregion

        static void RegisterHandlePosition(NodePort port, Rect rect)
        {
            var portPos = rect.center;
            NodeEditorCache.PortPositions[port] = portPos;
        }

        static void ApplyPropertyAttributes(IEnumerable<PropertyAttribute> propertyAttributes, bool usePropertyAttributes,
            out string toolTip, out float spacePadding)
        {
            spacePadding = 0;
            toolTip = null;

            foreach (var attr in propertyAttributes)
            {
                switch (attr)
                {
                    case SpaceAttribute attribute when usePropertyAttributes:
                        GUILayout.Space(attribute.height);
                        break;
                    case SpaceAttribute attribute:
                        spacePadding += attribute.height;
                        break;
                    case HeaderAttribute attribute when usePropertyAttributes:
                        {
                            //GUI Values are from https://github.com/Unity-Technologies/UnityCsReference/blob/master/Editor/Mono/ScriptAttributeGUI/Implementations/DecoratorDrawers.cs
                            var height = (EditorGUIUtility.singleLineHeight * 1.5f) - EditorGUIUtility.standardVerticalSpacing;
                            var position = GUILayoutUtility.GetRect(0, height); //Layout adds standardVerticalSpacing after rect so we subtract it.
                            position.yMin += EditorGUIUtility.singleLineHeight * 0.5f;
                            position = EditorGUI.IndentedRect(position);
                            GUI.Label(position, attribute.header, EditorStyles.boldLabel);
                            break;
                        }
                    case HeaderAttribute _:
                        spacePadding += EditorGUIUtility.singleLineHeight * 1.5f;
                        break;
                    case TooltipAttribute attribute:
                        toolTip = attribute.tooltip;
                        break;
                }
            }
        }

        static void GetAttributeData<T>(SerializedProperty property, NodePort port, out AttributeData attrData) where T : Attribute, IInputOutputAttribute
        {
            attrData = default;
            attrData.ShowBacking = ShowBackingValue.Unconnected;
            attrData.DynamicPortList = false;

            if (NodeEditorUtilities.GetCachedAttribute(port.Node.GetType(), property.name, out T attr))
            {
                attrData.Attribute = attr;
                attrData.DynamicPortList = attr.DynamicPortList;
                attrData.ShowBacking = attr.BackingValue;
            }

            attrData.UsePropertyAttributes = attrData.DynamicPortList ||
                       attrData.ShowBacking == ShowBackingValue.Never ||
                       (attrData.ShowBacking == ShowBackingValue.Unconnected && port.IsConnected);
        }

        static Type GetType(SerializedProperty property)
        {
            var parentType = property.serializedObject.targetObject.GetType();
            var fi = parentType.GetFieldInfo(property.name);
            return fi.FieldType;
        }

        /// <summary> Make a simple port field. </summary>
        public static void PortField(NodePort port, params GUILayoutOption[] options)
            => PortField(null, port, options);

        /// <summary> Make a simple port field. </summary>
        static void PortField(GUIContent label, NodePort port, params GUILayoutOption[] options)
        {
            if (port == null)
                return;
            if (options == null)
                options = new[] { GUILayout.MinWidth(30) };
            Vector2 position;
            var content = label ?? new GUIContent(ObjectNames.NicifyVariableName(port.FieldName));

            switch (port.Direction)
            {
                // If property is an input, display a regular property field and put a port handle on the left side
                case IO.Input:
                    {
                        // Display a label
                        EditorGUILayout.LabelField(content, options);

                        var rect = GUILayoutUtility.GetLastRect();
                        position = rect.position - new Vector2(k_portSize, 0);
                        break;
                    }
                // If property is an output, display a text label and put a port handle on the right side
                case IO.Output:
                    {
                        // Display a label
                        EditorGUILayout.LabelField(content, NodeEditorResources.OutputPort, options);

                        var rect = GUILayoutUtility.GetLastRect();
                        position = rect.position + new Vector2(rect.width, 0);
                        break;
                    }
                default: throw new ArgumentOutOfRangeException();
            }
            PortField(position, port);
        }

        /// <summary> Make a simple port field. </summary>
        internal static void PortField(Vector2 position, NodePort port)
        {
            if (port == null)
                return;

            var rect = new Rect(position, new Vector2(k_portSize, k_portSize));

            var editor = NodeEditorCache.GetEditor(port.Node, NodeEditorWindow.Current);

            var backgroundColor = editor.GetTint();
            var col = NodeEditorWindow.Current.GraphEditor.GetPortColor(port);
            DrawPortHandle(rect, backgroundColor, col);

            // Register the handle position
            var portPos = rect.center;
            NodeEditorCache.PortPositions[port] = portPos;
        }


        /// <summary> Is this port part of a DynamicPortList? </summary>
        public static bool IsDynamicPortListPort(NodePort port)
        {
            var parts = port.FieldName.Split(' ');
            if (parts.Length != 2)
                return false;
            return k_reorderableListCache.TryGetValue((Object)port.Node, out var cache)
                   && cache.TryGetValue(parts[0], out _);
        }

        #region Currently Unused

        /// <summary> Add a port field to previous layout element. </summary>
        public static void AddPortField(NodePort port)
        {
            if (port == null)
                return;
            var rect = GUILayoutUtility.GetLastRect();
            switch (port.Direction)
            {
                // If property is an input, display a regular property field and put a port handle on the left side
                case IO.Input:
                    rect.position -= new Vector2(k_portSize, 0);
                    break;
                // If property is an output, display a text label and put a port handle on the right side
                case IO.Output:
                    rect.position += new Vector2(rect.width, 0);
                    break;
                default: throw new ArgumentOutOfRangeException();
            }

            rect.size = new Vector2(k_portSize, k_portSize);

            var editor = NodeEditorCache.GetEditor(port.Node, NodeEditorWindow.Current);
            var backgroundColor = editor.GetTint();
            var col = NodeEditorWindow.Current.GraphEditor.GetPortColor(port);
            DrawPortHandle(rect, backgroundColor, col);

            // Register the handle position
            var portPos = rect.center;
            NodeEditorCache.PortPositions[port] = portPos;
        }

        /// <summary> Draws an input and an output port on the same line </summary>
        public static void PortPair(NodePort input, NodePort output)
        {
            GUILayout.BeginHorizontal();
            PortField(input, GUILayout.MinWidth(0));
            PortField(output, GUILayout.MinWidth(0));
            GUILayout.EndHorizontal();
        }
        #endregion
    }
}
