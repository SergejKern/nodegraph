﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

using nodegraph.Editor.Operations;
using Object = UnityEngine.Object;

namespace nodegraph.Editor.Utility
{
    public class ReorderablePortList : ReorderableList
    {
        // ReSharper disable once SuggestBaseTypeForParameter
        public ReorderablePortList(List<NodePort> dynamicPorts,
            string fieldName,
            SerializedProperty arrayData,
            Type type, SerializedObject serializedObject,
            IO io, ConnectionType connectionType, TypeConstraint typeConstraint)
            : base(dynamicPorts, null, true, true, true, true)
        {
            m_fieldName = fieldName;
            m_arrayData = arrayData;
            m_type = type;
            m_serializedObject = serializedObject;
            m_io = io;
            m_connectionType = connectionType;
            m_typeConstraint = typeConstraint;

            var nodeObj = serializedObject.targetObject;
            m_node = (INode)nodeObj;

            m_label = arrayData != null ? arrayData.displayName
                : ObjectNames.NicifyVariableName(m_fieldName);

            drawElementCallback = ReorderableListDrawElementCallback;
            elementHeightCallback = ReorderableListElementHeightCallback;
            drawHeaderCallback = ReorderableListDrawHeaderCallback;
            onSelectCallback = ReorderableListOnSelectCallback;
            onReorderCallback = ReorderableListOnReorderCallback;
            onAddCallback = ReorderableListOnAddCallback;
            onRemoveCallback = ReorderableListOnRemoveCallback;

            if (HasArrayData)
                AddDynamicPortsFromArray();
        }

        void AddDynamicPortsFromArray()
        {
            var dynamicPortCount = DynamicPorts.Count;
            while (dynamicPortCount < m_arrayData.arraySize)
            {
                // Add dynamic port post-fixed with an index number
                var newName = m_arrayData.name + " 0";
                var i = 0;
                while (m_node.HasPort(newName))
                    newName = m_arrayData.name + " " + (++i);
                if (m_io == IO.Output)
                    m_node.AddDynamicOutput(m_type, m_connectionType, m_typeConstraint, newName);
                else
                    m_node.AddDynamicInput(m_type, m_connectionType, m_typeConstraint, newName);

                EditorUtility.SetDirty(NodeObj);
                dynamicPortCount++;
            }

            while (m_arrayData.arraySize < dynamicPortCount)
                m_arrayData.InsertArrayElementAtIndex(m_arrayData.arraySize);

            m_serializedObject.ApplyModifiedProperties();
            m_serializedObject.Update();
        }

        static int m_reorderableListIndex = -1;

        readonly string m_fieldName;
        readonly SerializedProperty m_arrayData;
        readonly Type m_type;
        readonly SerializedObject m_serializedObject;
        readonly IO m_io;
        readonly ConnectionType m_connectionType;
        readonly TypeConstraint m_typeConstraint;

        readonly INode m_node;
        readonly string m_label;

        List<NodePort> DynamicPorts => list as List<NodePort>;
        bool HasArrayData => m_arrayData != null && m_arrayData.isArray;
        Object NodeObj => m_serializedObject.targetObject;


        #region ReorderableList Callbacks
        void ReorderableListOnRemoveCallback(ReorderableList rl)
        {
            var indexedPorts = m_node.DynamicPorts().Select(x =>
            {
                var split = x.FieldName.Split(' ');
                if (split.Length != 2 || split[0] != m_fieldName)
                    return new { index = -1, port = (NodePort)null };
                return int.TryParse(split[1], out var i)
                    ? new { index = i, port = x }
                    : new { index = -1, port = (NodePort)null };
            }).Where(x => x.port != null);
            list = indexedPorts.OrderBy(x => x.index).Select(x => x.port).ToList();

            var idx = rl.index;

            if (DynamicPorts[idx] == null)
                Debug.LogWarning("No port found at index " + idx + " - Skipped");
            else if (DynamicPorts.Count <= idx)
                Debug.LogWarning("DynamicPorts[" + idx + "] out of range. Length was " + DynamicPorts.Count +
                                 " - Skipped");
            else
            {

                // Clear the removed ports connections
                DynamicPorts[idx].ClearConnections();
                // Move following connections one step up to replace the missing connection
                for (var k = idx + 1; k < DynamicPorts.Count; k++)
                {
                    foreach (var c in DynamicPorts[k].Connections)
                    {
                        var other = DynamicPorts[k].GetConnectedPort(c);
                        NodePortOperations.Disconnect(DynamicPorts[k], other);
                        NodePortOperations.Connect(DynamicPorts[k - 1], other);
                    }
                }
                // Remove the last dynamic port, to avoid messing up the indexing
                m_node.RemoveDynamicPort(DynamicPorts[DynamicPorts.Count - 1].FieldName);
                m_serializedObject.Update();
                EditorUtility.SetDirty(NodeObj);
            }

            // ReSharper disable once PossibleNullReferenceException
            if (!HasArrayData || m_arrayData.propertyType == SerializedPropertyType.String)
                return;
            if (m_arrayData.arraySize <= idx)
            {
                Debug.LogWarning("Attempted to remove array index " + idx + " where only " + m_arrayData.arraySize + " exist - Skipped");
                Debug.Log(rl.list[0]);
                return;
            }
            m_arrayData.DeleteArrayElementAtIndex(idx);
            // Error handling. If the following happens too often, file a bug report at https://github.com/Siccity/xNode/issues
            if (DynamicPorts.Count <= m_arrayData.arraySize)
            {
                while (DynamicPorts.Count <= m_arrayData.arraySize)
                    m_arrayData.DeleteArrayElementAtIndex(m_arrayData.arraySize - 1);
                Debug.LogWarning("Array size exceeded dynamic ports size. Excess items removed.");
            }
            m_serializedObject.ApplyModifiedProperties();
            m_serializedObject.Update();

        }

        void ReorderableListOnAddCallback(ReorderableList _)
        {
            // Add dynamic port post-fixed with an index number
            var newName = m_fieldName + " 0";
            var i = 0;
            while (m_node.HasPort(newName)) newName = m_fieldName + " " + (++i);

            if (m_io == IO.Output)
                m_node.AddDynamicOutput(m_type, m_connectionType, TypeConstraint.None, newName);
            else
                m_node.AddDynamicInput(m_type, m_connectionType, m_typeConstraint, newName);

            m_serializedObject.Update();

            EditorUtility.SetDirty(NodeObj);
            if (HasArrayData)
            {
                Debug.Assert(m_arrayData != null, nameof(m_arrayData) + " != null");
                m_arrayData.InsertArrayElementAtIndex(m_arrayData.arraySize);
            }
            m_serializedObject.ApplyModifiedProperties();
        }

        void ReorderableListOnReorderCallback(ReorderableList rl)
        {
            bool hasRect, hasNewRect;
            Rect rect, newRect;
            // Move up
            if (rl.index > m_reorderableListIndex)
            {
                for (var i = m_reorderableListIndex; i < rl.index; ++i)
                {
                    var port = m_node.GetPort(m_fieldName + " " + i);
                    var nextPort = m_node.GetPort(m_fieldName + " " + (i + 1));
                    NodePortOperations.SwapConnections(port, nextPort);

                    // Swap cached positions to mitigate twitching
                    hasRect = NodeEditorWindow.Current.PortConnectionPoints.TryGetValue(port.ID, out rect);
                    hasNewRect = NodeEditorWindow.Current.PortConnectionPoints.TryGetValue(nextPort.ID, out newRect);
                    NodeEditorWindow.Current.PortConnectionPoints[port.ID] = hasNewRect ? newRect : rect;
                    NodeEditorWindow.Current.PortConnectionPoints[nextPort.ID] = hasRect ? rect : newRect;
                }
            }
            // Move down
            else for (var i = m_reorderableListIndex; i > rl.index; --i)
            {
                var port = m_node.GetPort(m_fieldName + " " + i);
                var nextPort = m_node.GetPort(m_fieldName + " " + (i - 1));
                NodePortOperations.SwapConnections(port, nextPort);

                // Swap cached positions to mitigate twitching
                hasRect = NodeEditorWindow.Current.PortConnectionPoints.TryGetValue(port.ID, out rect);
                hasNewRect = NodeEditorWindow.Current.PortConnectionPoints.TryGetValue(nextPort.ID, out newRect);
                NodeEditorWindow.Current.PortConnectionPoints[port.ID] = hasNewRect ? newRect : rect;
                NodeEditorWindow.Current.PortConnectionPoints[nextPort.ID] = hasRect ? rect : newRect;
            }

            // Apply changes
            m_serializedObject.ApplyModifiedProperties();
            m_serializedObject.Update();

            // Move array data if there is any
            if (HasArrayData)
            {
                Debug.Assert(m_arrayData != null, nameof(m_arrayData) + " != null");
                m_arrayData.MoveArrayElement(m_reorderableListIndex, rl.index);
            }

            // Apply changes
            m_serializedObject.ApplyModifiedProperties();
            m_serializedObject.Update();
            NodeEditorWindow.Current.Repaint();
            EditorApplication.delayCall += NodeEditorWindow.Current.Repaint;
        }

        static void ReorderableListOnSelectCallback(ReorderableList rl) => m_reorderableListIndex = rl.index;

        void ReorderableListDrawHeaderCallback(Rect rect) => EditorGUI.LabelField(rect, m_label);

        float ReorderableListElementHeightCallback(int idx)
        {
            if (!HasArrayData)
                return EditorGUIUtility.singleLineHeight;
            // ReSharper disable once PossibleNullReferenceException
            if (m_arrayData.arraySize <= idx)
                return EditorGUIUtility.singleLineHeight;
            var itemData = m_arrayData.GetArrayElementAtIndex(idx);
            return EditorGUI.GetPropertyHeight(itemData);
        }

        void ReorderableListDrawElementCallback(Rect rect, int idx, bool isActive, bool isFocused)
        {
            var port = m_node.GetPort(m_fieldName + " " + idx);
            // ReSharper disable once PossibleNullReferenceException
            if (HasArrayData && m_arrayData.propertyType != SerializedPropertyType.String)
            {
                if (m_arrayData.arraySize <= idx)
                {
                    EditorGUI.LabelField(rect, "Array[" + idx + "] data out of range");
                    return;
                }
                var itemData = m_arrayData.GetArrayElementAtIndex(idx);
                EditorGUI.PropertyField(rect, itemData, true);
            }
            else
                EditorGUI.LabelField(rect, port != null ? port.FieldName : "");

            if (port == null)
                return;
            var pos = rect.position + (port.IsOutput
                ? new Vector2(rect.width + 6, 0)
                : new Vector2(-36, 0));

            NodeEditorGUILayout.PortField(pos, port);
        }
        #endregion
    }
}
