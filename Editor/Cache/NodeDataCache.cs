﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Core.Events;
using Core.Extensions;
using Core.Types;
using Core.Unity.Types;
using UnityEngine;
using nodegraph.Attributes;
using nodegraph.Editor.Operations;
using NodeOperations = nodegraph.Operations.NodeOperations;


namespace nodegraph.Editor.Cache
{
    // todo: use NodeDataCache!!
    /// <summary> pre-caches reflection data in editor so we won't have to do it runtime </summary>
    public class NodeDataCache : Singleton<NodeDataCache>, IEventListener<NodeOperations.UpdatePorts>
    {
        protected override void Init()
        {
            base.Init();
            EventMessenger.AddListener(this);
        }
        public override void DestroyData()
        {
            base.DestroyData();
            EventMessenger.RemoveListener(this);
        }

        SerializedDictionary<Type, List<NodePort>> m_portDataCache;
        bool Initialized => m_portDataCache != null;

        /// <summary> Update static ports and dynamic ports managed by DynamicPortLists to reflect class fields. </summary>
        public void UpdatePorts(INode node)
        {
            if (!Initialized) 
                BuildCache();

            if (node?.NodeData == null)
                return;

            var staticPorts = new Dictionary<string, NodePort>();
            var removedPorts = new Dictionary<string, List<NodePort>>();
            var nodeType = node.NodeData.GetType();

            var dynamicListPorts = new List<NodePort>();

            var ports = node.Graph.GetPorts(node);

            CollectStaticPorts(nodeType, staticPorts);
            CleanUpdate(ports, staticPorts, removedPorts, dynamicListPorts);
            AddMissingPorts(node, ports, staticPorts, removedPorts);
            UpdateDynamicPortConstraints(dynamicListPorts, staticPorts);

            node.Graph.UpdatePorts(node, ports);
        }

        static void UpdateDynamicPortConstraints(IEnumerable<NodePort> dynamicListPorts, 
            IReadOnlyDictionary<string, NodePort> staticPorts)
        {
            // Finally, make sure dynamic list port settings correspond to the settings of their "backing port"
            foreach (var listPort in dynamicListPorts)
            {
                // At this point we know that ports here are dynamic list ports
                // which have passed name/"backing port" checks, ergo we can proceed more safely.
                var backingPortName = listPort.FieldName.Split(' ')[0];
                var backingPort = staticPorts[backingPortName];

                //Debug.Log($"updating dynamic port constraints.. {listPort.FieldName}\n" +
                //          $"from {listPort.ValueType} to {GetBackingValueType(backingPort.ValueType)}\n" +
                //          $"from {listPort.Direction} to {backingPort.Direction}\n" +
                //          $"from {listPort.ConnectionType} to {backingPort.ConnectionType}\n" +
                //          $"from {listPort.TypeConstraint} to {backingPort.TypeConstraint}\n");

                // Update port constraints. Creating a new port instead will break the editor, mandating the need for setters.
                listPort.ValueType = GetBackingValueType(backingPort.ValueType);
                listPort.Direction = backingPort.Direction;
                listPort.ConnectionType = backingPort.ConnectionType;
                listPort.TypeConstraint = backingPort.TypeConstraint;
            }
        }

        static void AddMissingPorts(INode node, 
            ICollection<NodePort> ports, Dictionary<string, NodePort> staticPorts, 
            IReadOnlyDictionary<string, List<NodePort>> removedPorts)
        {
            //Debug.Log($"{nameof(AddMissingPorts)}");

            foreach (var staticPort in staticPorts.Values)
            {
                if (!ports.Where(p => string.Equals(p.FieldName, staticPort.FieldName)).IsNullOrEmpty())
                    continue;
                var port = new NodePort(staticPort, node);
                //If we just removed the port, try re-adding the connections
                if (removedPorts.TryGetValue(staticPort.FieldName, out var reconnectConnections))
                {
                    foreach (var otherPort in reconnectConnections)
                    {
                        if (otherPort == null) 
                            continue;
                        if (!port.CanConnectTo(otherPort)) 
                            continue;

                        //Debug.Log($"port reconnecting.. {port.FieldName} {otherPort.FieldName}");
                        NodePortOperations.Connect(port, otherPort);
                    }
                }

                ports.Add(port);
            }
        }

        /// <summary>
        /// Cleanup port dict - Remove non-existing static ports - update static port types
        /// AND update dynamic ports (albeit only those in lists) too, in order to enforce proper serialization.
        /// Loop through current node ports
        /// </summary>
        static void CleanUpdate(IList<NodePort> ports, 
            IReadOnlyDictionary<string, NodePort> staticPorts, 
            IDictionary<string, List<NodePort>> removedPorts, 
            ICollection<NodePort> dynamicListPorts)
        {
            for (var idx = ports.Count-1; idx > 0; idx--)
            {
                var port = ports[idx];
                // If port still exists, check it it has been changed
                if (staticPorts.TryGetValue(port.FieldName, out var staticPort))
                {
                    // If port exists but with wrong settings, remove it. Re-add it later.
                    if (port.IsDynamic || port.Direction != staticPort.Direction ||
                        port.ConnectionType != staticPort.ConnectionType ||
                        port.TypeConstraint != staticPort.TypeConstraint)
                    {
                        //Debug.Log($"port with wrong settings, removing.. {port.FieldName}");

                        // If port is not dynamic and direction hasn't changed, add it to the list so we can try reconnecting the ports connections.
                        if (!port.IsDynamic && port.Direction == staticPort.Direction)
                            removedPorts.Add(port.FieldName, port.GetConnections());
                        port.ClearConnections();
                        ports.RemoveAt(idx);
                    }
                    else port.ValueType = staticPort.ValueType;
                }
                // If port doesn't exist anymore, remove it
                else if (port.IsStatic)
                {
                    //Debug.Log($"port doesn't exist, removing.. {port.FieldName}");

                    port.ClearConnections();
                    ports.RemoveAt(idx);
                }
                // If the port is dynamic and is managed by a dynamic port list, flag it for reference updates
                else if (IsDynamicListPort(port))
                    dynamicListPorts.Add(port);
            }
        }

        void CollectStaticPorts(Type nodeType, IDictionary<string, NodePort> staticPorts)
        {
            //Debug.Log($"{nameof(CollectStaticPorts)} {nodeType}");
            if (!m_portDataCache.TryGetValue(nodeType, out var typePortCache)) 
                return;

            for (var i = 0; i < typePortCache.Count; i++)
            {
                //Debug.Log($"adding static port {typePortCache[i].FieldName}");
                staticPorts.Add(typePortCache[i].FieldName, m_portDataCache[nodeType][i]);
            }
        }

        /// <summary>
        /// Extracts the underlying types from arrays and lists, the only collections for dynamic port lists
        /// currently supported. If the given type is not applicable (i.e. if the dynamic list port was not
        /// defined as an array or a list), returns the given type itself. 
        /// </summary>
        static Type GetBackingValueType(Type portValType)
        {
            if (portValType.HasElementType)
                return portValType.GetElementType();
            if (portValType.IsGenericType && portValType.GetGenericTypeDefinition() == typeof(List<>))
                return portValType.GetGenericArguments()[0];
            return portValType;
        }

        /// <summary>Returns true if the given port is in a dynamic port list.</summary>
        static bool IsDynamicListPort(NodePort port)
        {
            // Ports flagged as "dynamicPortList = true" end up having a "backing port" and a name with an index, but we have
            // no guarantee that a dynamic port called "output 0" is an element in a list backed by a static "output" port.
            // Thus, we need to check for attributes... (but at least we don't need to look at all fields this time)
            var fieldNameParts = port.FieldName.Split(' ');
            if (fieldNameParts.Length != 2) return false;

            var backingPortInfo = port.Node.GetType().GetField(fieldNameParts[0]);
            if (backingPortInfo == null) return false;

            var attributes = backingPortInfo.GetCustomAttributes(true);
            return attributes.Any(x
                => x is InputAttribute inputAttribute && inputAttribute.DynamicPortList 
                   || x is OutputAttribute outputAttribute && outputAttribute.DynamicPortList);
        }

        /// <summary> Cache node types </summary>
        void BuildCache()
        {
            m_portDataCache = new SerializedDictionary<Type, List<NodePort>>();
            var baseType = typeof(INodeData);
            var nodeTypes = new List<Type>();
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            // Loop through assemblies and add node types to list
            foreach (var assembly in assemblies)
            {
                // Skip certain dlls to improve performance
                var assemblyName = assembly.GetName().Name;
                var index = assemblyName.IndexOf('.');
                if (index != -1) assemblyName = assemblyName.Substring(0, index);
                switch (assemblyName)
                {
                    // The following assemblies, and sub-assemblies (eg. UnityEngine.UI) are skipped
                    case "UnityEditor":
                    case "UnityEngine":
                    case "System":
                    case "mscorlib":
                    case "Microsoft":
                        continue;
                    default:
                        nodeTypes.AddRange(assembly.GetTypes().Where(t => !t.IsAbstract && baseType.IsAssignableFrom(t)).ToArray());
                        break;
                }
            }

            foreach (var nt in nodeTypes) 
                CachePorts(nt);
        }

        static IEnumerable<FieldInfo> GetNodeFields(Type nodeType)
        {
            var fieldInfo = new List<FieldInfo>(nodeType.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance));

            // GetFields doesn't return inherited private fields, so walk through base types and pick those up
            var tempType = nodeType;
            while ((tempType = tempType.BaseType) != null)
            {
                if (tempType == typeof(Node) || tempType == typeof(ScriptableObject))
                    break;
                Debug.Assert(tempType != null, nameof(tempType) + " != null");
                var parentFields = tempType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
                foreach (var parentField in parentFields)
                {
                    var field = parentField;
                    if (fieldInfo.TrueForAll(x => x.Name != field.Name)) 
                        fieldInfo.Add(parentField);
                }
            }
            return fieldInfo;
        }

        void CachePorts(Type nodeType)
        {
            var fieldInfo = GetNodeFields(nodeType);

            foreach (var fi in fieldInfo)
            {
                //Get InputAttribute and OutputAttribute
                var attributes = fi.GetCustomAttributes(true);
                var inputAttributes = attributes.FirstOrDefault(x => x is InputAttribute) as InputAttribute;
                var outputAttributes = attributes.FirstOrDefault(x => x is OutputAttribute) as OutputAttribute;

                if (inputAttributes == null && outputAttributes == null) 
                    continue;

                if (inputAttributes != null && outputAttributes != null) 
                    Debug.LogError("Field " + fi.Name + " of type " + nodeType.FullName + " cannot be both input and output.");
                else
                {
                    if (!m_portDataCache.ContainsKey(nodeType)) 
                        m_portDataCache.Add(nodeType, new List<NodePort>());

                    m_portDataCache[nodeType].Add(new NodePort(fi));
                }
            }
        }

        /// <summary> Update static ports and dynamic ports managed by DynamicPortLists to reflect class fields.
        /// This happens automatically on enable or on redrawing a dynamic port list. </summary>
        public void OnEvent(NodeOperations.UpdatePorts @event) 
            => UpdatePorts(@event.Node);
    }
}
