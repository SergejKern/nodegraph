﻿using Core.Unity.Interface;
using UnityEditor;
using UnityEngine;
using nodegraph.Editor.Interface;

namespace nodegraph.Editor.Graph
{
    /// <summary> Base class for nodes and graphs </summary>
    /// <typeparam name="TObj">Runtime Type. The ScriptableObject this can be an editor for (eg. Node or NodeGraph) </typeparam>
    public abstract class NodeEditorBase<TObj> : INodeEditorBase where TObj : class, IScriptableObject
    {
        public SerializedObject SerializedObject { get; private set; }

        public ScriptableObject TargetObj => m_target as ScriptableObject;

        protected NodeEditorWindow m_window;
        protected TObj m_target;

        public void Init(NodeEditorWindow window, ScriptableObject target)
        {
            SetTargetData(window, target);
            OnCreate();
        }

        public virtual void SetTargetData(NodeEditorWindow window, ScriptableObject target)
        {
            m_window = window;
            m_target = target as TObj;
            if (SerializedObject == null)
                SerializedObject = new SerializedObject(TargetObj);
        }

        /// <summary> Called on creation, after references have been set </summary>
        protected virtual void OnCreate(){}
    }
}