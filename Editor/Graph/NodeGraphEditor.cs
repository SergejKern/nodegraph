﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Editor.Extensions;
using Core.Editor.PopupWindows;
using Core.Editor.Utility;
using Core.Runtime.Interface;
using Core.Types;
using UnityEditor;
using UnityEngine;
using nodegraph.Attributes;
using nodegraph.Data;
using nodegraph.Editor.Attributes;
using nodegraph.Editor.Data;
using nodegraph.Editor.Interface;
using nodegraph.Editor.Operations;
using nodegraph.Editor.Settings;
using nodegraph.Editor.Utility;

using Object = UnityEngine.Object;
using PrefSettings = nodegraph.Editor.Settings.Settings;

namespace nodegraph.Editor.Graph
{
    /// <summary> Base class to derive custom Node Graph editors from. Use this to override how graphs are drawn in the editor. </summary>
    [CustomNodeGraphEditor(typeof(INodeGraph))]
    public class NodeGraphEditor : NodeEditorBase<INodeGraph>, INodeGraphEditor
    {
        /// <summary> Are we currently renaming a node? </summary>
        protected bool m_isRenaming;

        public virtual void OnGUI() { }

        /// <summary> Called when opened by NodeEditorWindow </summary>
        public virtual void OnOpen() { }

        /// <summary> Called when NodeEditorWindow gains focus </summary>
        public virtual void OnWindowFocus() { }

        /// <summary> Called when NodeEditorWindow loses focus </summary>
        public virtual void OnWindowFocusLost() { }

        public virtual GridTextures GetGridTextures()
        {
            var settings = NodeEditorPreferences.GetSettings();
            return new GridTextures()
            {
                GridTexture = settings.GridTexture,
                CrossTexture = settings.CrossTexture
            };
        }

        /// <summary> Return default settings for this graph type. This is the settings the user will load if no previous settings have been saved. </summary>
        public virtual PrefSettings GetDefaultPreferences()
            => new PrefSettings();

        /// <summary> Returns context node menu path. Null or empty strings for hidden nodes. </summary>
        protected virtual string GetNodeMenuName(Type type)
        {
            // Check if type has the CreateNodeMenuAttribute
            return NodeEditorUtilities.GetAttribute(type, out CreateNodeMenuAttribute attribute)
                ? attribute.MenuName
                : NodeEditorUtilities.NodeDefaultPath(type);
        }

        /// <summary> The order by which the menu items are displayed. </summary>
        protected virtual int GetNodeMenuOrder(Type type)
        {
            //Check if type has the CreateNodeMenuAttribute
            return NodeEditorUtilities.GetAttribute(type, out CreateNodeMenuAttribute attribute)
                ? attribute.Order : 0;
        }

        /// <summary> Add items for the context menu when right-clicking this node. Override to add custom menu items. </summary>
        public virtual void AddContextMenuItems(GenericMenu menu)
        {
            var mousePos = Event.current.mousePosition;
            var gridPos = NodeEditorWindow.Current.WindowToGridPosition(mousePos);
            AddNodeTypesContextMenu(menu, mousePos, gridPos);
            AddDefaultContextMenu(menu, gridPos);
        }
        
        public void AddContextMenuItemsWithNodesSelected(GenericMenu menu)
        {
            var mousePos = Event.current.mousePosition;
            var gridPos = NodeEditorWindow.Current.WindowToGridPosition(mousePos);

            menu.AddSeparator("");
            menu.AddItem(new GUIContent("Nest Graph"), false, () 
                => NodeEditorWindow.Current.ShowCreateNestedGraphPopup(gridPos));
        }

        protected void AddDefaultContextMenu(GenericMenu menu, Vector2 gridPos)
        {
            menu.AddSeparator("");

            if (NodeEditorWindow.CopyBuffer != null && NodeEditorWindow.CopyBuffer.Length > 0)
                menu.AddItem(new GUIContent("Paste"), false, () => NodeEditorWindow.Current.PasteNodes(gridPos));
            else
                menu.AddDisabledItem(new GUIContent("Paste"));

            menu.AddItem(new GUIContent("Preferences"), false, NodeEditorReflection.OpenPreferences);
            menu.AddCustomContextMenuItems(m_target);
        }

        protected void AddNodeTypesContextMenu(GenericMenu menu, Vector2 mousePos, Vector2 gridPos)
        {
            using (var nodeTypes = SimplePool<List<Type>>.I.GetScoped())
            {
                var allNodeTypes = NodeEditorReflection.NodeTypes.OrderBy(GetNodeMenuOrder).ToArray();
                foreach (var type in allNodeTypes)
                {
                    //Get node context menu path
                    var path = GetNodeMenuName(type);
                    if (string.IsNullOrEmpty(path))
                        continue;

                    // Check if user is allowed to add more of given node type
                    var disallowed = false;
                    if (NodeEditorUtilities.GetAttribute(type, out DisallowMultipleNodesAttribute disallowAttribute))
                    {
                        var type1 = type;
                        var typeCount = m_target.NodeObjs.Count(x => x != null && x.GetType() == type1);
                        if (typeCount >= disallowAttribute.Max)
                            disallowed = true;
                    }

                    if(!disallowed)
                        nodeTypes.Obj.Add(type);
                }

                EditorExtensions.GetNamesAndNamespacesForTypes(nodeTypes.Obj, out var names, out var namespaces);
                var popup = EditorExtensions.CreateTypeSelectorPopup(nodeTypes.Obj, names, namespaces, new EditorExtensions.MenuFromNamespace(), 
                    (type) => AddNode(gridPos, (Type) type));

                menu.AddItem(new GUIContent("Add Node"), false, 
                    () => PopupWindow.Show(new Rect(mousePos, SelectTypesPopup.WindowSize), popup));
            }
        }

        protected void AddNode(Vector2 gridPos, Type type, string name = null)
        {
            var node = CreateNode(type, gridPos, name);
            NodeEditorWindow.Current.AutoConnect(node);
        }

        /// <summary> Returned gradient is used to color noodles </summary>
        /// <param name="output"> The output this noodle comes from. Never null. </param>
        /// <param name="input"> The output this noodle comes from. Can be null if we are dragging the noodle. </param>
        protected virtual Gradient GetNoodleGradient(Connection c)
        {
            var input = m_target.GetPort(c.To);
            var output = m_target.GetPort(c.From);
            return GetNoodleGradient(input, output);
        }

        protected virtual Gradient GetNoodleGradient(NodePort input, NodePort output)
        {
            var grad = new Gradient();

            Debug.Assert(output != null);
            // If dragging the noodle, draw solid, slightly transparent
            if (input == null)
            {
                var a = GetTypeColor(output.ValueType);
                grad.SetKeys(
                    new[] { new GradientColorKey(a, 0f) },
                    new[] { new GradientAlphaKey(0.6f, 0f) }
                );
            }
            // If normal, draw gradient fading from one input color to the other
            else
            {
                var a = GetTypeColor(output.ValueType);
                var b = GetTypeColor(input.ValueType);
                // If any port is hovered, tint white
                if (m_window.HoveredPort == output || m_window.HoveredPort == input)
                {
                    a = Color.Lerp(a, Color.white, 0.8f);
                    b = Color.Lerp(b, Color.white, 0.8f);
                }
                grad.SetKeys(
                    new[] { new GradientColorKey(a, 0f), new GradientColorKey(b, 1f) },
                    new[] { new GradientAlphaKey(1f, 0f), new GradientAlphaKey(1f, 1f) }
                );
            }
            return grad;
        }
        public virtual NoodleData GetNoodleData(NodePort input, NodePort output) =>
            new NoodleData()
            {
                Thickness = 5f,
                Path = NodeEditorPreferences.GetSettings().NoodlePath,
                Stroke = NodeEditorPreferences.GetSettings().NoodleStroke,
                Gradient = GetNoodleGradient(input, output)
            };
        public virtual NoodleData GetNoodleData(Connection c)
        {
            var input = m_target.GetPort(c.To);
            var output = m_target.GetPort(c.From);

            return GetNoodleData(input, output);
        }

        /// <summary> Returned color is used to color ports </summary>
        public virtual Color GetPortColor(PortID id) => GetPortColor(m_target.GetPort(id));
        public virtual Color GetPortColor(NodePort port) => port == null ? default : GetTypeColor(port.ValueType);

        /// <summary> Returns generated color for a type. This color is editable in preferences </summary>
        protected virtual Color GetTypeColor(Type type) => NodeEditorPreferences.GetTypeColor(type);

        /// <summary> Override to display custom tooltips </summary>
        public virtual string GetPortTooltip(PortID id) => GetPortTooltip(m_target.GetPort(id));
        public virtual string GetPortTooltip(NodePort port)
        {
            if (port == null)
                return "";

            var portType = port.ValueType;
            var tooltip = portType.PrettyName();
            if (!port.IsOutput)
                return tooltip;

            var obj = port.Node.GetOutputValue(port.ID);
            tooltip += " = " + (obj != null ? obj.ToString() : "null");
            return tooltip;
        }

        /// <summary> Deal with objects dropped into the graph through DragAndDrop </summary>
        public virtual void OnDropObjects(Object[] objects)
        {
            if (GetType() != typeof(NodeGraphEditor))
                Debug.Log("No OnDropObjects override defined for " + GetType());
        }

        /// <summary> Create a node and save it in the graph asset </summary>
        protected virtual INode CreateNode(Type dataType, Vector2 position, string name = null)
        {
            Undo.RecordObject(TargetObj, "Create Node");

            var needsWrapper = !typeof(INode).IsAssignableFrom(dataType);
            var nodeType = needsWrapper ? typeof(WrapperNode) : dataType;
            var node = m_target.AddNode(nodeType, name);

            var nodeData = needsWrapper
                ? (INodeData) ScriptableObject.CreateInstance(dataType)
                : (INodeData) node;

            if (needsWrapper)
            {
                nodeData.TryFixAssetName();
                ((WrapperNode) node).Editor_SetNodeData(nodeData);
                ((IOptionalNode) nodeData).SetNode(node);
            }

            Undo.RegisterCreatedObjectUndo((Object) node, "Create Node");
            node.Position = position;

            if (node.name.Trim() == "" || name == null)
                node.name = nodeData.DefaultName();

            var mainPathValid = !string.IsNullOrEmpty(AssetDatabase.GetAssetPath(TargetObj));
            if (mainPathValid)
            {
                AssetDatabase.AddObjectToAsset((ScriptableObject) node, TargetObj);
                if (needsWrapper)
                    AssetDatabase.AddObjectToAsset((ScriptableObject) nodeData, TargetObj);
            }
            else 
                Debug.LogError("Main Path of Graph not valid!");

            if (NodeEditorPreferences.GetSettings().AutoSave)
                AssetDatabase.SaveAssets();

            NodeEditorWindow.RepaintAll();
            return node;
        }

        /// <summary> Creates a copy of the original node in the graph </summary>
        public virtual INode CopyNode(INode original)
        {
            Undo.RecordObject(TargetObj, "Duplicate Node");
            var node = m_target.CopyNode(original);
            Undo.RegisterCreatedObjectUndo((Object)node, "Duplicate Node");
            node.name = original.name;

            // todo: copy NodeData if different asset
            AssetDatabase.AddObjectToAsset((ScriptableObject) node, TargetObj);
            if (NodeEditorPreferences.GetSettings().AutoSave)
                AssetDatabase.SaveAssets();

            return node;
        }

        /// <summary> Return false for nodes that can't be removed </summary>
        public virtual bool CanRemove(INode node)
        {
            // Check graph attributes to see if this node is required
            var graphType = m_target.GetType();
            var attributes = Array.ConvertAll(
                graphType.GetCustomAttributes(typeof(RequireNodeAttribute), true),
                x => x as RequireNodeAttribute);

            if (!attributes.Any(x => x.Requires(node.GetType())))
                return true;
            return m_target.NodeObjs.Count(x => x.GetType() == node.GetType()) > 1;
        }

        /// <summary> Safely remove a node and all its connections. </summary>
        public virtual void RemoveNode(INode node)
        {
            if (!CanRemove(node))
                return;

            // Remove the node
            Undo.RecordObject((ScriptableObject)node, "Delete Node");
            Undo.RecordObject(TargetObj, "Delete Node");

            foreach (var port in node.Ports())
                foreach (var conn in port.GetConnections())
                    Undo.RecordObject((Object)conn.Node, "Delete Node");

            m_target.RemoveNode(node);
            // todo: remove NodeData if different asset
            Undo.DestroyObjectImmediate((ScriptableObject)node);

            if (NodeEditorPreferences.GetSettings().AutoSave)
                AssetDatabase.SaveAssets();
        }
    }
}
