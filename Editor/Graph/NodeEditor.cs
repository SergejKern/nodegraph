using UnityEditor;
using UnityEngine;
using nodegraph.Editor.Attributes;
using nodegraph.Editor.Data;
using nodegraph.Editor.Interface;

namespace nodegraph.Editor.Graph
{
    /// <summary> Base class to derive custom Node editors from. Use this to create your own custom inspectors and editors for your nodes. </summary>
    [CustomNodeEditor(typeof(INode))]
    public class NodeEditor : NodeEditorBase<INode>, INodeEditor
    {
        public SerializedObject SerializedObjectData => SerializedObject;
        public INode Target => m_target;
        protected INodeData NodeData => Target.NodeData;

        protected override void OnCreate()
        {
            base.OnCreate();
            DynamicConnectorUpdater.I.InitDynamicPorts(this);
        }

        public virtual void OnHeaderGUI() => this.DefaultHeader();
        /// <summary> Draws standard field editors for all public fields </summary>
        public virtual void OnBodyGUI() => this.DefaultBodyGUI(SerializedObject, m_target);
        public virtual int GetWidth() => this.DefaultGetWidth(NodeData);

        /// <summary> Returns color for target node </summary>
        public virtual Color GetTint() => this.DefaultGetTint(NodeData);
        public virtual NodeStyles GetStyles() => this.DefaultGetStyles();

        /// <summary> Override to display custom node header tooltips </summary>
        public virtual string GetHeaderTooltip() => null;

        /// <summary> Add items for the context menu when right-clicking this node. Override to add custom menu items. </summary>
        public virtual void AddContextMenuItems(GenericMenu menu) => this.DefaultAddContextMenuItems(menu);

        /// <summary> Rename the node asset. This will trigger a reimport of the node. </summary>
        public void Rename(string newName) => this.DefaultRename(newName);

        public virtual void OnNodeUpdated(){}
    }
}