using UnityEditor;
using UnityEngine;
using nodegraph.Editor.Attributes;
using nodegraph.Editor.Data;
using nodegraph.Editor.Interface;

namespace nodegraph.Editor.Graph
{
    /// <summary> Base class to derive custom Node editors from. Use this to create your own custom inspectors and editors for your nodes. </summary>
    [CustomNodeEditor(typeof(WrapperNode))]
    public class WrapperNodeEditor : NodeEditorBase<WrapperNode>, INodeEditor
    {
        public SerializedObject SerializedObjectData { get; private set; }
        public INode Target => m_target;
        INodeData NodeData => m_target.NodeData;

        public override void SetTargetData(NodeEditorWindow window, ScriptableObject target)
        {
            base.SetTargetData(window, target);

            if (SerializedObjectData == null)
                SerializedObjectData = new SerializedObject((ScriptableObject) NodeData);
        }

        protected override void OnCreate()
        {
            base.OnCreate();
            DynamicConnectorUpdater.I.InitDynamicPorts(this);
        }

        public void OnHeaderGUI() => this.DefaultHeader();

        /// <summary> Draws standard field editors for all public fields </summary>
        public void OnBodyGUI() => this.DefaultBodyGUI(SerializedObjectData, m_target);

        public int GetWidth() => this.DefaultGetWidth(NodeData);

        /// <summary> Returns color for target node </summary>
        public Color GetTint() => this.DefaultGetTint(NodeData);
        public NodeStyles GetStyles() => this.DefaultGetStyles();


        /// <summary> Override to display custom node header tooltips </summary>
        public string GetHeaderTooltip() 
            => null;

        /// <summary> Add items for the context menu when right-clicking this node. Override to add custom menu items. </summary>
        public void AddContextMenuItems(GenericMenu menu) => this.DefaultAddContextMenuItems(menu);

        /// <summary> Rename the node asset. This will trigger a reimport of the node. </summary>
        public void Rename(string newName)=> this.DefaultRename(newName);


        /// <summary> Called after this node's name has changed. </summary>
        public void OnNodeUpdated(){}
    }
}