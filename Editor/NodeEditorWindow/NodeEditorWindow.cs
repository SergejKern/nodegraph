using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;
using System;
using nodegraph.Data;
using nodegraph.Editor.Cache;
using nodegraph.Editor.Settings;
using nodegraph.Editor.Utility;
using Object = UnityEngine.Object;
using PortRectDict = System.Collections.Generic.Dictionary<nodegraph.Data.PortID, UnityEngine.Rect>;

using nodegraph.Editor.Operations;

namespace nodegraph.Editor
{
    //[InitializeOnLoad]
    public partial class NodeEditorWindow : EditorWindow
    {
        #region public fields
        public static NodeEditorWindow Current;
        public INodeGraph Graph;
        #endregion

        #region Serialized Fields
        [SerializeField] PortID[] m_references = new PortID[0];
        [SerializeField] Rect[] m_rects = new Rect[0];
        #endregion

        #region properties
        public bool IsVisible { get; private set; }

        /// <summary> Stores node positions for all nodePorts. </summary>
        public PortRectDict PortConnectionPoints { get; } = new PortRectDict();

        static Settings.Settings Settings => NodeEditorPreferences.GetSettings();

        Dictionary<INode, Vector2> NodeSizes { get; } = new Dictionary<INode, Vector2>();

        Func<bool> IsDocked => m_isDocked ?? (m_isDocked = this.GetIsDockedDelegate());
        Func<bool> m_isDocked;

        Vector2 PanOffset
        {
            get => m_panOffset;
            set
            {
                m_panOffset = value;
                Repaint();
            }
        }
        Vector2 m_panOffset;

        float Zoom
        {
            get => m_zoom;
            set
            {
                m_zoom = Mathf.Clamp(value, Settings.MinZoom, Settings.MaxZoom);
                Repaint();
            }
        }
        float m_zoom = 1;
        #endregion

        #region Init
        [InitializeOnLoadMethod]
        static void OnLoad()
        {
            Selection.selectionChanged -= OnSelectionChanged;
            Selection.selectionChanged += OnSelectionChanged;
        }

        /// <summary> Create editor window </summary>
        public static NodeEditorWindow Init()
        {
            var w = CreateInstance<NodeEditorWindow>();
            w.titleContent = new GUIContent("xNode");
            w.wantsMouseMove = true;
            w.Show();
            return w;
        }

        [OnOpenAsset(0)]
        public static bool OnOpen(int instanceID, int line)
        {
            if (!(EditorUtility.InstanceIDToObject(instanceID) is INodeGraph nodeGraph)) 
                return false;
            Open(nodeGraph);
            return true;
        }

        /// <summary>Open the provided graph in the NodeEditor</summary>
        public static NodeEditorWindow Open(INodeGraph graph)
        {
            if (graph == null) 
                return null;
            var w = (NodeEditorWindow) GetWindow(typeof(NodeEditorWindow), false, "xNode", true);
            w.wantsMouseMove = true;
            w.Graph = graph;
            foreach (var node in graph.NodeObjs) 
                NodeDataCache.I.UpdatePorts(node as INode);

            return w;
        }
        #endregion

        #region Unity Hooks
        void OnDisable() => CachePortConnectionPoints();
        void OnEnable() => ReloadPortConnectionPoints();
        
        void OnFocus()
        {
            Current = this;
            ValidateGraphEditor();
            if (GraphEditor != null)
            {
                GraphEditor.OnWindowFocus();
                if (Settings.AutoSave) 
                    AssetDatabase.SaveAssets();
            }

            m_dragThreshold = Math.Max(1f, Screen.width / 1000f);
        }

        void OnLostFocus() => GraphEditor?.OnWindowFocusLost();

        void OnBecameVisible() => IsVisible = true;
        void OnBecameInvisible()=> IsVisible = false;
        #endregion
        
        #region Save Load Cache
        public void Save()
        {
            if (AssetDatabase.Contains((Object) Graph))
            {
                EditorUtility.SetDirty((Object) Graph);
                if (Settings.AutoSave) 
                    AssetDatabase.SaveAssets();
            }
            else SaveAs();
        }

        void SaveAs()
        {
            var path = EditorUtility.SaveFilePanelInProject("Save NodeGraph", "NewNodeGraph", "asset", "");
            if (string.IsNullOrEmpty(path)) 
                return;
            var existingGraph = AssetDatabase.LoadAssetAtPath<Object>(path);
            if (existingGraph != null) 
                AssetDatabase.DeleteAsset(path);
            AssetDatabase.CreateAsset((Object) Graph, path);
            EditorUtility.SetDirty((Object) Graph);
            if (Settings.AutoSave) 
                AssetDatabase.SaveAssets();
        }

        void CachePortConnectionPoints()
        {
            // Cache portConnectionPoints before serialization starts
            var count = PortConnectionPoints.Count;
            m_references = new PortID[count];
            m_rects = new Rect[count];
            var index = 0;
            foreach (var portConnectionPoint in PortConnectionPoints)
            {
                m_references[index] = portConnectionPoint.Key;
                m_rects[index] = portConnectionPoint.Value;
                index++;
            }
        }

        void ReloadPortConnectionPoints()
        {
            // Reload portConnectionPoints if there are any

            var length = m_references.Length;
            if (length != m_rects.Length)
                return;
            for (var i = 0; i < length; i++)
            {
                var nodePort = m_references[i].GetNodePort();
                if (nodePort != null)
                    PortConnectionPoints.Add(nodePort.ID, m_rects[i]);
            }
        }
        #endregion

        #region Grid Utility
        public Vector2 WindowToGridPosition(Vector2 windowPosition) => (windowPosition - (position.size * 0.5f) - (PanOffset / Zoom)) * Zoom;

        Vector2 GridToWindowPosition(Vector2 gridPosition) => (position.size * 0.5f) + (PanOffset / Zoom) + (gridPosition / Zoom);

        Rect GridToWindowRectNoClipped(Rect gridRect)
        {
            gridRect.position = GridToWindowPositionNoClipped(gridRect.position);
            return gridRect;
        }

        Rect GridToWindowRect(Rect gridRect)
        {
            gridRect.position = GridToWindowPosition(gridRect.position);
            gridRect.size /= Zoom;
            return gridRect;
        }

        Vector2 GridToWindowPositionNoClipped(Vector2 gridPosition)
        {
            var center = position.size * 0.5f;
            // UI Sharpness complete fix - Round final offset not panOffset
            var xOffset = Mathf.Round(center.x * Zoom + (PanOffset.x + gridPosition.x));
            var yOffset = Mathf.Round(center.y * Zoom + (PanOffset.y + gridPosition.y));
            return new Vector2(xOffset, yOffset);
        }
        #endregion

        #region Selection
        /// <summary> Handle Selection Change events</summary>
        static void OnSelectionChanged()
        {
            var selectObj = Selection.activeObject;
            if (!(selectObj is INodeGraph nodeGraph))
                return;
            if (AssetDatabase.Contains(selectObj)) 
                return;
            if (Settings.OpenOnCreate) 
                Open(nodeGraph);
        }

        static void SelectNode(Object node, bool add)
        {
            if (add)
            {
                var selection = new List<Object>(Selection.objects) {node};
                //Debug.Log($"Selected {selection}");
                Selection.objects = selection.ToArray();
            }
            else
            {
                //Debug.Log($"Selected {node}");
                Selection.objects = new[] { node };
            }
        }

        static void DeselectNode(Object node)
        {
            var selection = new List<Object>(Selection.objects);
            selection.Remove(node);
            //Debug.Log($"Selected {selection}");
            Selection.objects = selection.ToArray();
        }
        #endregion

        /// <summary> Repaint all open NodeEditorWindows. </summary>
        public static void RepaintAll()
        {
            var windows = UnityEngine.Resources.FindObjectsOfTypeAll<NodeEditorWindow>();
            foreach (var w in windows) 
                w.Repaint();
        }
        
        /// <summary> Make sure the graph editor is assigned and to the right object </summary>
        void ValidateGraphEditor()
        {
            var graphEditor = NodeEditorCache.GetEditor(Graph, this);
            if (GraphEditor == graphEditor || graphEditor == null) 
                return;
            GraphEditor = graphEditor;
            graphEditor.OnOpen();
        }
        void DraggableWindow(int windowID) => GUI.DragWindow();
    }
}
