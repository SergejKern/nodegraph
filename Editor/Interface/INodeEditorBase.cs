﻿
using UnityEditor;
using UnityEngine;

namespace nodegraph.Editor.Interface
{
    public interface INodeEditorBase
    {
        ScriptableObject TargetObj { get; }
        SerializedObject SerializedObject { get; }

        void SetTargetData(NodeEditorWindow window, ScriptableObject target);
        void Init(NodeEditorWindow window, ScriptableObject target);
    }
}