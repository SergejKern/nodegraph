﻿using UnityEditor;
using UnityEngine;
using nodegraph.Editor.Data;

namespace nodegraph.Editor.Interface
{
    public interface INodeEditor : INodeEditorBase
    {
        INode Target { get; }
        SerializedObject SerializedObjectData { get; }

        void OnHeaderGUI();

        /// <summary> Draws standard field editors for all public fields </summary>
        void OnBodyGUI();

        int GetWidth();

        /// <summary> Returns color for target node </summary>
        Color GetTint();

        NodeStyles GetStyles();

        /// <summary> Override to display custom node header tooltips </summary>
        string GetHeaderTooltip();

        /// <summary> Add items for the context menu when right-clicking this node. Override to add custom menu items. </summary>
        void AddContextMenuItems(GenericMenu menu);

        /// <summary> Rename the node asset. This will trigger a reimport of the node. </summary>
        void Rename(string newName);

        void OnNodeUpdated();
    }
}