﻿using UnityEditor;
using UnityEngine;
using nodegraph.Data;
using nodegraph.Editor.Data;

namespace nodegraph.Editor.Interface
{
    public interface INodeGraphEditor : INodeEditorBase
    {
        void OnGUI();

        /// <summary> Called when opened by NodeEditorWindow </summary>
        void OnOpen();
        /// <summary> Called when NodeEditorWindow gains focus </summary>
        void OnWindowFocus();
        /// <summary> Called when NodeEditorWindow loses focus </summary>
        void OnWindowFocusLost();

        GridTextures GetGridTextures();
        /// <summary> Return default settings for this graph type. This is the settings the user will load if no previous settings have been saved. </summary>
        Settings.Settings GetDefaultPreferences();

        /// <summary> Add items for the context menu when right-clicking this graph background. Override to add custom menu items. </summary>
        void AddContextMenuItems(GenericMenu menu);
        void AddContextMenuItemsWithNodesSelected(GenericMenu menu);

        NoodleData GetNoodleData(Connection c);
        NoodleData GetNoodleData(NodePort input, NodePort output);

        /// <summary> Returned color is used to color ports </summary>
        Color GetPortColor(PortID id);
        Color GetPortColor(NodePort id);
        /// <summary> Override to display custom tooltips </summary>
        string GetPortTooltip(PortID port);
        string GetPortTooltip(NodePort id);
        /// <summary> Deal with objects dropped into the graph through DragAndDrop </summary>
        void OnDropObjects(Object[] objects);
        /// <summary> Creates a copy of the original node in the graph </summary>
        INode CopyNode(INode original);
        /// <summary> Return false for nodes that can't be removed </summary>
        bool CanRemove(INode node);
        /// <summary> Safely remove a node and all its connections. </summary>
        void RemoveNode(INode node);
    }
}
