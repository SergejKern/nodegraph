﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Interface;
using Core.Unity.Interface;
using UnityEngine;
using nodegraph.Data;
using nodegraph.Operations;
using Object = UnityEngine.Object;

using static nodegraph.Operations.NodeOperations;

namespace nodegraph.Editor.Operations
{
    public static class NodeGraphOperations
    {
        public static void Add(this INodeGraph @this, INode n)
            => CollectionExtensions.Add(ref @this.NodeObjs, (ScriptableObject) n);
        /// <summary> Add a node to the graph by type (convenience method - will call the System.Type version) </summary>
        public static T AddNode<T>(this INodeGraph @this, string name = null) where T : INode
            => (T) @this.AddNode(typeof(T), name);
        /// <summary> Add a node to the graph by type </summary>
        public static INode AddNode(this INodeGraph @this, Type type, string name = null)
        {
            // hotfix to operations
            GraphHotfix = @this;
            var node = (INode) ScriptableObject.CreateInstance(type);
            
            if (name != null && node is INameable nameable)
                nameable.SetName(name);

            node.Graph = @this;
            @this.Add(node);
            return node;
        }

        static void Remove(this INodeGraph @this, INode n)
            => CollectionExtensions.Remove(ref @this.NodeObjs, (ScriptableObject) n);

        /// <summary> Safely remove a node and all its connections </summary>
        /// <param name="this">the NodeGraph</param>
        /// <param name="node"> The node to remove </param>
        public static void RemoveNode(this INodeGraph @this, INode node)
        {
            node.ClearConnections();
            @this.Remove(node);
            if (Application.isPlaying) 
                Object.Destroy((Object) node);
        }

        public static bool Contains(this INodeGraph @this, INode node)
            => @this.NodeObjs.Contains((ScriptableObject) node);

        // read only
        public static INode[] R_INodes(this INodeGraph @this) => @this.NodeObjs.Cast<INode>().ToArray();

        
        /// <summary> Creates a copy of the original node in the graph </summary>
        public static INode CopyNode(this INodeGraph @this, INode original)
        {
            GraphHotfix = @this;
            var node = (INode) Object.Instantiate((Object) original);
            node.Graph = @this;
            node.ClearConnections();
            @this.Add(node);
            return node;
        }

        ///// <summary> Create a new deep copy of this graph </summary>
        //public static T Copy<T>(this T @this) where T : ScriptableObject, INodeGraph, IParentAsset
        //{
        //    // Instantiate a new nodegraph instance
        //    var graph = (T) Object.Instantiate((ScriptableObject) @this);
        //    // Instantiate all nodes inside the graph
        //    for (var i = 0; i < @this.ChildAssets.Length; i++)
        //    {
        //        if (@this.ChildAssets[i] == null)
        //            continue;
        //        GraphHotfix = graph;
        //        var child = Object.Instantiate(@this.ChildAssets[i]);
        //        if (child is INode n)
        //            n.Graph = graph;
        //        graph.ChildAssets[i] = child;
        //    }
        //    // Redirect all connections
        //    foreach (var sc in graph.NodeObjs)
        //    {
        //        if (_!(sc is INode n))
        //            continue;
        //        foreach (var port in n.Ports())
        //            port.Redirect(@this.R_INodes(), graph.R_INodes());
        //    }
        //    return graph;
        //}

        /// <summary> Remove all child assets from the graph </summary>
        public static void Clear<T>(this T @this) where T : ScriptableObject, INodeGraph
            => @this.Editor_Clear();

        #region New Port Handling

        public static void Add(this INodeGraph @this, NodePort port) => @this.NodePorts.Add(port);
        public static void Remove(this INodeGraph @this, NodePort port) => @this.NodePorts.Remove(port);

        public static NodePort GetPort(this INodeGraph @this, PortID port) => @this.NodePorts.FirstOrDefault(p => p.ID == port);

        //Debug.Log(m_ports.Count);//foreach (var p in m_ports)//    Debug.Log($"{p} {p.Node == node}");

        public static IList<NodePort> GetPorts(this INodeGraph @this, INode node) 
            => @this.NodePorts.Where(p => p.Node == node).ToList();

        public static void UpdatePorts(this INodeGraph @this, INode node, IEnumerable<NodePort> ports)
        {
            @this.NodePorts.RemoveAll(p => p.Node == node);
            @this.NodePorts.AddRange(ports);
        }


        public static void AddConnection(this INodeGraph @this, Connection connection) => CollectionExtensions.Add(ref @this.Connections, connection);
        public static void RemoveConnection(this INodeGraph @this, Connection connection) => CollectionExtensions.Remove(ref @this.Connections, connection);

        public static void Disconnect(this INodeGraph @this, PortID aID, PortID bID)
        {
            //Debug.LogWarning($"Disconnected {aID} {bID} \n{Environment.StackTrace}");
            CollectionExtensions.RemoveAll(ref @this.Connections,
                c => c.From == aID && c.To == bID || c.From == bID && c.To == aID);
        }

        public static void RemoveConnections(this INodeGraph @this, IEnumerable<Connection> removeConnections) 
            => @this.Connections = @this.Connections.Where(c => !removeConnections.Contains(c)).ToArray();

        public static List<Vector2> GetReroutePoints(this INodeGraph @this, Connection c)
        {
            if (@this.ReroutePoints.TryGetValue(c, out var reroutes))
                return reroutes;
            reroutes = new List<Vector2>();
            @this.ReroutePoints.Add(c, reroutes);
            return reroutes;
        }
        #endregion
    }
}
