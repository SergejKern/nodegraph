using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using nodegraph.Data;
using nodegraph.Operations;
using Object = UnityEngine.Object;

namespace nodegraph.Editor.Operations
{
    public static class NodeOperations
    {
        /// <summary> Iterate over all outputs on this node. </summary>
        public static IEnumerable<NodePort> Outputs(this INode n) => n.Ports().Where(port => port.IsOutput);

        /// <summary> Iterate over all inputs on this node. </summary>
        public static IEnumerable<NodePort> Inputs(this INode n) => n.Ports().Where(port => port.IsInput);

        /// <summary> Iterate over all dynamic ports on this node. </summary>
        public static IEnumerable<NodePort> DynamicPorts(this INode n) => n.Ports().Where(port => port.IsDynamic);

        /// <summary> Iterate over all dynamic outputs on this node. </summary>
        public static IEnumerable<NodePort> DynamicOutputs(this INode n) => n.Ports().Where(port => port.IsDynamic && port.IsOutput);

        /// <summary> Iterate over all dynamic inputs on this node. </summary>
        public static IEnumerable<NodePort> DynamicInputs(this INode n) => n.Ports().Where(port => port.IsDynamic && port.IsInput);

        #region Dynamic Ports
        /// <summary> Convenience function. </summary>
        public static NodePort AddDynamicInput(this INode @this, Type type, 
            ConnectionType connectionType = ConnectionType.Override, 
            TypeConstraint typeConstraint = TypeConstraint.None, 
            string fieldName = null) 
            => @this.AddDynamicPort(type, IO.Input, 
                connectionType, typeConstraint, fieldName);

        /// <summary> Convenience function. </summary>
        public static NodePort AddDynamicOutput(this INode @this, Type type, 
            ConnectionType connectionType = ConnectionType.Override, 
            TypeConstraint typeConstraint = TypeConstraint.None, 
            string fieldName = null) 
            => @this.AddDynamicPort(type, IO.Output, 
                connectionType, typeConstraint, fieldName);

        /// <summary> Add a dynamic, serialized port to this  </summary>
        /// <seealso cref="AddDynamicInput"/>
        /// <seealso cref="AddDynamicOutput"/>
        public static NodePort AddDynamicPort(this INode @this, Type type, IO direction, 
            ConnectionType connectionType = ConnectionType.Override, 
            TypeConstraint typeConstraint = TypeConstraint.None, 
            string fieldName = null)
        {
            if (fieldName == null)
            {
                fieldName = "dynamicInput_0";
                var i = 0;
                while (@this.HasPort(fieldName)) 
                    fieldName = "dynamicInput_" + (++i);
            }
            else if (@this.HasPort(fieldName))
            {
                Debug.LogWarning("Port '" + fieldName + "' already exists in " + @this.name, (Object) @this);
                return @this.GetPort(fieldName);
            }

            var port = new NodePort(fieldName, type, direction, connectionType, typeConstraint, @this);
            @this.Graph.Add(port);
            return port;
        }

        /// <summary> Remove an dynamic port from the node </summary>
        public static void RemoveDynamicPort(this INode @this, string fieldName)
        {
            var dynamicPort = @this.GetPort(fieldName);
            if (dynamicPort == null) 
                throw new ArgumentException("port " + fieldName + " doesn't exist");

            @this.RemoveDynamicPort(@this.GetPort(fieldName));
        }

        /// <summary> Remove an dynamic port from the node </summary>
        public static void RemoveDynamicPort(this INode @this, NodePort port)
        {
            if (port == null) 
                throw new ArgumentNullException(nameof(port));
            if (port.IsStatic) 
                throw new ArgumentException("cannot remove static port");

            port.ClearConnections();
            @this.Graph.Remove(port);
        }

        /// <summary> Removes all dynamic ports from the node </summary>
        public static void ClearDynamicPorts(this INode @this)
        {
            var dynamicPorts = new List<NodePort>(@this.DynamicPorts());
            foreach (var port in dynamicPorts) 
                @this.RemoveDynamicPort(port);
        }
        #endregion

        #region Ports
        public static IEnumerable<NodePort> Ports(this INode @this) => @this.Graph.GetPorts(@this);

        /// <summary> Returns output port which matches fieldName </summary>
        public static NodePort GetOutputPort(this INode @this, string fieldName)
        {
            var port = @this.GetPort(fieldName);
            if (port == null || port.Direction != IO.Output)
                return null;
            return port;
        }

        /// <summary> Returns input port which matches fieldName </summary>
        public static NodePort GetInputPort(this INode @this, string fieldName)
        {
            var port = @this.GetPort(fieldName);
            if (port == null || port.Direction != IO.Input) 
                return null;
            return port;
        }

        /// <summary> Returns port which matches fieldName </summary>
        public static NodePort GetPort(this INode @this, string fieldName) 
            => @this.Ports().FirstOrDefault(p => string.Equals(p.FieldName, fieldName));

        public static bool HasPort(this INode @this, string fieldName)
            => @this.GetPort(fieldName) != null;

        public static NodePort GetNodePort(this PortID @this) =>
            @this.Node is INode n
                ? n.GetPort(@this.FieldName)
                : null;
        #endregion

        public static IEnumerable<Connection> Connections(this INode @this) => @this.Ports().SelectMany(p => p.Connections);
        /// <summary> Disconnect everything from this node </summary>
        public static void ClearConnections(this INode @this)
        {
            foreach (var port in @this.Ports()) 
                port.ClearConnections();
        }
    }
}
