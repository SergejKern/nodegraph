﻿using System;
using System.Collections.Generic;
using Core.Extensions;
using nodegraph.Attributes;
using nodegraph.Editor.Interface;
using UnityEditor;

namespace nodegraph.Editor.Data
{
    // These help to map fields to dynamic outputs faster
    public struct ConnectorListMemory
    {
        public ConnectorPropData PropData;
        public ConnectorPort[] Ports;
    }

    public struct ConnectorMemory
    {
        public ConnectorPropData PropData;
        public ConnectorPort Port;
    }

    public struct ConnectorPort
    {
        public string FieldName;
        public INodeData Data;
    }

    public readonly struct ConnectorPropData
    {
        public ConnectorPropData(string propPath, NodeConnectorAttributeData attr, bool isList)
        {
            PropertyPath = propPath;
            IO = attr.IO;
            ConnectorType = attr.ConnectorType; //typeof(INodeConnector<>).MakeGenericType(listAttr.ConnectorType),
            m_relativePropPathToObject = attr.RelativePropPathToObject;
            m_relativePropPathToName = attr.RelativePropPathToName;

            FieldNameFormat = isList
                ? $"{PropertyPath}[{{0}}].{m_relativePropPathToObject}"
                : $"{PropertyPath}.{m_relativePropPathToObject}";

            SyncData = attr.SyncData;
        }
        public ConnectorPropData(ConnectorPropData other)
        {
            IO = other.IO;
            PropertyPath = other.PropertyPath;
            ConnectorType = other.ConnectorType;
            m_relativePropPathToName = other.m_relativePropPathToName;
            m_relativePropPathToObject = other.m_relativePropPathToObject;
            FieldNameFormat = other.FieldNameFormat;
            SyncData = other.SyncData;
        }

        public readonly IO IO;
        public readonly string PropertyPath;
        public readonly Type ConnectorType;

        public readonly string FieldNameFormat;

        readonly string m_relativePropPathToName;
        readonly string m_relativePropPathToObject;

        public readonly bool SyncData;


        public void GetNameAndObjProp(SerializedProperty elementProp, out string fieldName,
            out SerializedProperty objProp)
        {
            fieldName = GetFieldName(elementProp, -1);
            objProp = GetObjectProp(elementProp);
        }
        public void GetNameAndObjProp(SerializedProperty elementProp, int i, out string fieldName,
            out SerializedProperty objProp)
        {
            fieldName = GetFieldName(elementProp, i);
            objProp = GetObjectProp(elementProp);
        }

        public string GetFieldName(SerializedProperty elementProp, int i) =>
            m_relativePropPathToName.IsNullOrEmpty()
                ? string.Format(FieldNameFormat, i)
                : elementProp.FindPropertyRelative(m_relativePropPathToName).stringValue;
        public SerializedProperty GetObjectProp(SerializedProperty elementProp) =>
            m_relativePropPathToObject.IsNullOrEmpty()
                ? elementProp : elementProp.FindPropertyRelative(m_relativePropPathToObject);
    }

    public struct NodeConnectorMemory
    {
        public INodeEditor Editor;
        public List<ConnectorListMemory> ListMemory;
        public List<ConnectorMemory> Memory;

        public static NodeConnectorMemory Default => new NodeConnectorMemory()
        {
            Memory = new List<ConnectorMemory>(),
            ListMemory = new List<ConnectorListMemory>()
        };
    }
}