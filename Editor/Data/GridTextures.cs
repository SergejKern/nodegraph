﻿using UnityEngine;

namespace nodegraph.Editor.Data
{
    public struct GridTextures
    {
        public Texture2D GridTexture;
        public Texture2D CrossTexture;

        public float Width => GridTexture.width;
        public float Height => GridTexture.height;
    }
}