﻿using UnityEngine;
using nodegraph.Editor.Resources;

namespace nodegraph.Editor.Data
{
    public struct NodeStyles
    {
        public GUIStyle NodeHeader;
        public GUIStyle Tooltip;

        public GUIStyle BGDefault;
        public GUIStyle BGSelected;
        public GUIStyle SelectedGlow;

        public bool IsValid => BGDefault != null;

        public static NodeStyles Default
        {
            get
            {
                
                var defaultBorder = new RectOffset(32, 32, 32, 32);
                var defaultPadding = new RectOffset(16, 16, 4, 16);
                var noPadding = new RectOffset();

                var nodeHeader = new GUIStyle
                {
                    alignment = TextAnchor.MiddleCenter,
                    fontStyle = FontStyle.Bold,
                    normal = {textColor = Color.white}
                };

                var nodeBody = new GUIStyle
                {
                    normal = {background = NodeEditorResources.NodeBody},
                    border = defaultBorder,
                    padding = defaultPadding
                };
                var nodeHighlight = new GUIStyle
                {
                    normal = {background = NodeEditorResources.NodeBody},
                    border = defaultBorder,
                    padding = noPadding
                };
                var highlightGlow = new GUIStyle
                {
                    normal = {background = NodeEditorResources.NodeHighlight},
                    border = defaultBorder,
                    padding = defaultPadding
                };

                var tooltip = new GUIStyle("helpBox") {alignment = TextAnchor.MiddleCenter};

                return new NodeStyles()
                {
                    BGDefault = nodeBody,
                    BGSelected = nodeHighlight,
                    NodeHeader = nodeHeader,
                    SelectedGlow = highlightGlow,
                    Tooltip = tooltip
                };
            }
        }

    }
}