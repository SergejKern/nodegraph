﻿using UnityEngine;
using nodegraph.Data;
using nodegraph.Editor.Operations;

namespace nodegraph.Editor.Data
{
    public readonly struct RerouteReference
    {
        public readonly Connection Connection;
        public readonly int PointIndex;

        public RerouteReference(Connection c, int pointIndex)
        {
            Connection = c;
            PointIndex = pointIndex;
        }

        public void InsertPoint(Vector2 pos) { Connection.Graph.GetReroutePoints(Connection).Insert(PointIndex, pos); }
        public void SetPoint(Vector2 pos) { Connection.Graph.GetReroutePoints(Connection)[PointIndex] = pos; }
        public void RemovePoint() { Connection.Graph.GetReroutePoints(Connection).RemoveAt(PointIndex); }
        public Vector2 GetPoint() { return Connection.Graph.GetReroutePoints(Connection)[PointIndex]; }
    }
}