﻿using UnityEngine;
using nodegraph.Editor.Enums;

namespace nodegraph.Editor.Data
{
    public struct NoodleData
    {
        public float Thickness;

        public NoodlePath Path;
        public NoodleStroke Stroke;
        public Gradient Gradient;
    }
}