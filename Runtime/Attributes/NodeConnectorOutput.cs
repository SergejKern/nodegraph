using System;

namespace nodegraph.Attributes
{
    public class NodeConnectorListMultiAttribute : Attribute
    {
        public readonly Type DataType;
        public readonly string[] RelativePaths;
         
        public NodeConnectorListMultiAttribute(Type dataType, string[] relativePaths)
        {
            DataType = dataType;
            RelativePaths = relativePaths;
        }
    }

    public class NodeConnectorListAttribute : NodeConnectorAttribute
    {
        public NodeConnectorListAttribute(IO io, Type connector, 
            string propPathName = null, 
            string propPathObject = null,
            bool syncData = true) : base(io, connector, propPathName, propPathObject, syncData)
        { }
    }

    public class NodeConnectorAttribute : Attribute
    {
        public NodeConnectorAttributeData Data { get; }
        public NodeConnectorAttribute(IO io,
            Type connector, 
            string propPathName = "", 
            string propPathObject = "",
            bool syncData = true)
        {
            Data = new NodeConnectorAttributeData()
            {
                IO = io,
                ConnectorType = connector,
                RelativePropPathToName = propPathName,
                RelativePropPathToObject = propPathObject,
                SyncData = syncData
            };
        }
    }

    public struct NodeConnectorAttributeData
    {
        public IO IO;
        public Type ConnectorType;
        public string RelativePropPathToName;
        public string RelativePropPathToObject;
        public bool SyncData;
    }
}
