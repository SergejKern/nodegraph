using System;

namespace nodegraph.Attributes
{
    /// <summary> Specify a width for this node type </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class NodeWidthAttribute : Attribute
    {
        public readonly int Width;
        /// <summary> Specify a width for this node type </summary>
        /// <param name="width"> Width </param>
        public NodeWidthAttribute(int width) 
            => Width = width;
    }
}