﻿using UnityEngine;

namespace nodegraph
{
    /// <summary> Lets you instantiate a node graph in the scene. This allows you to reference in-scene objects. </summary>
    public class SceneGraph : MonoBehaviour
    {
        public INodeGraph Graph;
    }

    /// <summary> Derive from this class to create a SceneGraph with a specific graph type. </summary>
    /// <example>
    /// <code>
    /// public class MySceneGraph : SceneGraph<MyGraph> {
    /// 	
    /// }
    /// </code>
    /// </example>
    public class SceneGraph<T> : SceneGraph where T : class, INodeGraph
    {
        public new T Graph
        {
            get => base.Graph as T;
            set => base.Graph = value;
        }
    }
}