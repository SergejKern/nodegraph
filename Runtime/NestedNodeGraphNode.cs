using nodegraph.Attributes;
using UnityEngine;

namespace nodegraph
{
    [NodeTint(0.5f, 0.5f, 0.25f),
     NodeWidth(300)]
    public class NestedNodeGraphNode : DynamicPortsNode
    {
        [SerializeField] ScriptableObject m_nestedGraph;

        public void SetGraph(NestedNodeGraph nestedGraph) => m_nestedGraph = nestedGraph;
    }
}