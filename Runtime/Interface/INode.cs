﻿using Core.Interface;
using Core.Unity.Interface;
using UnityEngine;
using nodegraph.Data;

namespace nodegraph
{
    public interface INode : IScriptableObject, INamed
    {
        INodeGraph Graph { get; set; }
        Vector2 Position { get; set; }
        INodeData NodeData { get; }

        void Init();

        /// <summary> Returns a value based on requested port output. Should be overridden in all derived nodes with outputs. </summary>
        /// <param name="port">The requested port.</param>
        object GetOutputValue(PortID port);

        /// <summary> Called after a connection between two Ports is created </summary>
        /// <param name="port">Port</param>
        void OnCreateConnection(PortID port);

        /// <summary> Called after a connection is removed from this port </summary>
        /// <param name="port">Output or Input</param>
        void OnRemoveConnection(PortID port);

#if UNITY_EDITOR
        string[] Editor_GUIExcludes { get; }
#endif
    }
}
