﻿using System.Collections.Generic;
using Core.Interface;
using Core.Unity.Interface;
using UnityEngine;

namespace nodegraph
{
    public interface INodeData : IScriptableObject, INamed
    {
        INode Node { get; }
    }

    public interface IOptionalNode : INodeData
    {
        void SetNode(INode n);
    }

    public interface IFullNode : INode, INodeData{}

    public interface INestedGraph
    {
        INodeGraph NestedGraph { get; }
    }

    public interface IGUIColor
    {
        Color Color { get; }
    }
}
