﻿using System;
using System.Collections.Generic;
using Core.Runtime.Interface;
using Core.Unity.Interface;
using UnityEngine;
using nodegraph.Data;
using static nodegraph.Operations.NodeOperations;

namespace nodegraph
{
    /// <summary>
    /// Base class for all nodes
    /// </summary>
    /// <example>
    /// Classes extending this class will be considered as valid nodes by nodegraph.
    /// <code>
    /// [System.Serializable]
    /// public class Adder : Node {
    ///     [Input] public float a;
    ///     [Input] public float b;
    ///     [Output] public float result;
    ///
    ///     // GetValue should be overridden to return a value for any specified output port
    ///     public override object GetValue(NodePort port) {
    ///         return a + b;
    ///     }
    /// }
    /// </code>
    /// </example>
    [Serializable]
    public abstract class Node : ScriptableObject, INode, IDependencies //IDefaultNameProvider
    {
        public abstract INodeData NodeData { get; }

        public virtual INodeGraph Graph
        {
            get => (INodeGraph) m_graph;
            set => m_graph = (ScriptableObject) value;
        }
        public Vector2 Position
        {
            get => m_position;
            set => m_position = value;
        }
        public virtual string Name => name;

#pragma warning disable 0649 // wrong warnings for SerializeField
        /// <summary> Parent <see cref="NodeGraph"/> </summary>
        [HideInInspector, SerializeField] ScriptableObject m_graph;
        /// <summary> Position on the <see cref="NodeGraph"/> </summary>
        [HideInInspector, SerializeField] Vector2 m_position;
#pragma warning restore 0649

        protected void OnEnable() => this.Default_OnEnable();

        /// <summary> Initialize node. Called on enable. </summary>
        public virtual void Init() { }

        //[ContextMenu("Clear Dynamic Ports")]
        //public void ClearDynamicPorts_Ctx() => this.ClearDynamicPorts();

        /// <summary> Returns a value based on requested port output. Should be overridden in all derived nodes with outputs. </summary>
        /// <param name="port">The requested port.</param>
        public virtual object GetOutputValue(PortID port)
        {
            Debug.LogWarning("No GetValue(NodePort port) override defined for " + GetType());
            return null;
        }

        /// <summary> Called after a connection between two Ports is created </summary>
        /// <param name="port">Output or Input</param>
        public virtual void OnCreateConnection(PortID port) { }

        /// <summary> Called after a connection is removed from this port </summary>
        /// <param name="port">Output or Input</param>
        public virtual void OnRemoveConnection(PortID port) { }


#if UNITY_EDITOR
        public static string GraphPropName => nameof(m_graph);

        public string[] Editor_GUIExcludes => new[] { "m_Script", nameof(m_graph), nameof(m_position) };
#endif
        // IDefaultNameProvider
        // public virtual string DefaultName => NodeDefaultName(GetType());

        public virtual IEnumerable<ScriptableObject> Dependencies
        {
            get
            {
                yield return NodeData as ScriptableObject;
            }
        }
    }
}
