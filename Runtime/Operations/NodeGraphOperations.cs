using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using UnityEngine;
using nodegraph.Data;

namespace nodegraph.Operations
{
    public static class NodeGraphOperations
    {
        #if UNITY_EDITOR
        /// <summary> Remove all child assets from the graph </summary>
        public static void Editor_Clear<T>(this T @this) where T : ScriptableObject, INodeGraph
        {
            foreach (var n in @this.NodeObjs)
                n.DestroyEx();

            @this.NodeObjs = new ScriptableObject[0];
        }
        #endif

        #region New Port Handling
        public static Connection GetConnectionFrom(this INodeGraph @this, PortID fromPort) 
            => @this.Connections.FirstOrDefault(c => c.From == fromPort);
        public static Connection GetConnectionTo(this INodeGraph @this, PortID toPort) 
            => @this.Connections.FirstOrDefault(c => c.To == toPort);
        public static IEnumerable<Connection> GetConnectionsFrom(this INodeGraph @this, PortID fromPort)
            => @this.Connections.Where(c => c.From == fromPort);
        public static IEnumerable<Connection> GetConnectionsTo(this INodeGraph @this, PortID toPort)
            => @this.Connections.Where(c => c.To == toPort);
        public static IEnumerable<Connection> GetConnectionsFor(this INodeGraph @this, PortID port) 
            => @this.Connections.Where(c => c.From == port || c.To == port);
        #endregion
    }
}