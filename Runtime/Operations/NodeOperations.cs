﻿using System;
using System.Linq;
using Core.Events;
using UnityEngine;

using nodegraph.Data;

namespace nodegraph.Operations
{
    public static class NodeOperations
    {
        public struct UpdatePorts
        {
            public INode Node;
        }

        /// <summary> Used during node instantiation to fix null/misconfigured graph during OnEnable/Init. Set it before instantiating a node. Will automatically be unset during OnEnable </summary>
        public static INodeGraph GraphHotfix;

        public const int DefaultNodeSize = 200;

        public static void Default_OnEnable(this INode n)
        {
#if UNITY_EDITOR
            if (GraphHotfix != null) 
                n.Graph = GraphHotfix;
            GraphHotfix = null;

            EventMessenger.TriggerEvent(new UpdatePorts(){ Node = n });
#endif

            n.Init();
        }

        ///// <summary> Checks all connections for invalid references, and removes them. </summary>
        //public static void VerifyConnections(this INode n)
        //{
        //    foreach (var port in n.Ports) 
        //        port.VerifyConnections();
        //}

        static PortID GetPortID(this INode @this, string fieldName) 
            => new PortID(){ Node = (ScriptableObject) @this, FieldName = fieldName };

        #region Inputs/Outputs

        /// <summary> Return input value for a specified port. Returns fallback value if no ports are connected </summary>
        /// <param name="this"></param>
        /// <param name="fieldName">Field name of requested input port</param>
        /// <param name="fallback">If no ports are connected, this value will be returned</param>
        public static T GetInputValue<T>(this INode @this, string fieldName, T fallback = default)
        {
            var portId = @this.GetPortID(fieldName);
            var connection = @this.Graph.GetConnectionTo(portId);

            if (connection.From.Node is INode otherNode) 
                return (T) otherNode.GetOutputValue(connection.From);
            return fallback;
        }

        public static bool TryGetInputValue<T>(this INode @this, string fieldName, out T val)
        {
            val = default;
            var portId = @this.GetPortID(fieldName);
            var connection = @this.Graph.GetConnectionTo(portId);

            if (!(connection.From.Node is INode otherNode))
                return false;

            val = (T) otherNode.GetOutputValue(connection.From);
            return true;
        }

        /// <summary> Return all input values for a specified port. Returns fallback value if no ports are connected </summary>
        /// <param name="this"></param>
        /// <param name="fieldName">Field name of requested input port</param>
        /// <param name="fallback">If no ports are connected, this value will be returned</param>
        public static T[] GetInputValues<T>(this INode @this, string fieldName, params T[] fallback)
        {
            var portId = @this.GetPortID(fieldName);
            var connections = @this.Graph.GetConnectionsTo(portId).ToList();
            if (!connections.Any())
                return fallback;

            var results = new T[connections.Count];
            for (var i = 0; i < connections.Count; i++)
            {
                var c = connections[i];
                if (c.From.Node is INode otherNode)
                    results[i] = (T) otherNode.GetOutputValue(c.From);
            }

            return results;
        }
        #endregion

        public static string NodeDefaultName(Type type)
        {
            var typeName = type.Name;

#if UNITY_EDITOR
            // Automatically remove redundant 'Node' postfix
            if (typeName.EndsWith("Node")) 
                typeName = typeName.Substring(0, 
                    typeName.LastIndexOf("Node", StringComparison.Ordinal));

            typeName = UnityEditor.ObjectNames.NicifyVariableName(typeName);
#endif
            return typeName;
        }
    }
}