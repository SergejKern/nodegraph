﻿using nodegraph.Attributes;
using UnityEngine;

namespace nodegraph
{
    [NodeTint(0f,0.5f,0.25f),
     NodeWidth(300)]
    public class DynamicPortsNode : Node, IFullNode
    {
        [SerializeField] NodePort[] m_nodePorts;

        public override INodeData NodeData => this;
        public INode Node => this;

        public void SaveDynamicNodes(NodePort[] ports) 
            => m_nodePorts = ports;
    }
}
