//using System;
//using System.Collections.Generic;
//using UnityEngine;
//using nodegraph.Operations;

//namespace XNode
//{
//    [Serializable]
//    internal class PortConnection
//    {
//        public string FieldName
//        {
//            get => m_fieldName;
//            set => m_fieldName = value;
//        }
//        public INode Node
//        {
//            get => (INode)m_node;
//            set => m_node = (ScriptableObject)value;
//        }

//        /// <summary> Extra connection path points for organization </summary>
//        public List<Vector2> ReroutePoints => m_reroutePoints;

//        public NodePort Port
//            => m_port ?? (m_port = GetPort());

//#pragma warning disable 0649 // wrong warnings for SerializeField
//        [SerializeField] string m_fieldName;
//        [SerializeField] ScriptableObject m_node;
//        [SerializeField] List<Vector2> m_reroutePoints = new List<Vector2>();
//#pragma warning restore 0649 // wrong warnings for SerializeField
//        NodePort m_port;

//        public PortConnection(NodePort port)
//        {
//            m_port = port;
//            Node = port.Node;
//            FieldName = port.FieldName;
//        }

//        /// <summary> Returns the port that this <see cref="PortConnection"/> points to </summary>
//        NodePort GetPort()
//        {
//            if (Node == null || string.IsNullOrEmpty(FieldName))
//                return null;
//            return Node.GetPort(FieldName);
//        }
//    }
//}