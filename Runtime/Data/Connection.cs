using System;

namespace nodegraph.Data
{
    [Serializable]
    public struct Connection
    {
        public PortID From;
        public PortID To;

        public static bool operator ==(Connection a, Connection b) => a.From == b.From;
        public static bool operator !=(Connection a, Connection b) => a.From != b.From;

        public INodeGraph Graph => ((From.Node != null ? From.Node : To.Node) as INode)?.Graph;

        public override string ToString() => $"|{From}->{To}|";
    }
}